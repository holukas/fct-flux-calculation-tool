import os

# automatic detection of working directory, test if running on RDS
# automatically detect folder where main.py resides
# currently not working on RDS, but works on local computer
abspath = os.path.abspath(__file__)  # directory of start_FCT.py
root_dir = os.path.join(os.path.dirname(abspath))  # dir from were the script was started
source_dir = os.path.join(os.path.dirname(abspath), 'FCT_FluxCalcTool')  # scripts and everything that is needed for calcs
# working_directory = os.path.dirname(abspath)
# os.chdir(source_dir)


import FCT_FluxCalcTool.python_scripts.main as main
main.main(root_dir=root_dir, source_dir=source_dir)
