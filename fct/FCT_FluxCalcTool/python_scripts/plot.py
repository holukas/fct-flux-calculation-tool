import matplotlib as mpl
mpl.use('Agg')  # when calling savefig() instead of show() the pop-up window won’t appear
import fnmatch
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import dates
from matplotlib.ticker import MultipleLocator
import datetime as dt
# from pandas.stats.moments import *
import sys
# import seaborn as sns


def plot_converted_raw(file_to_plot, destination_folder, use_rds):

    # # test:
    # plot_name = os.path.splitext(file_to_plot)[0]
    # contents = pd.read_csv(test_file_path, header=0)

    print("\n----------------------------")
    print("\n> CURRENTLY WORKING ON FILE: " + file_to_plot)
    plot_name = os.path.splitext(file_to_plot)[0]

    try:
        contents_head = pd.read_csv(file_to_plot, header=0, nrows=5)  # read only first 5 rows and header
    except:
        contents_head = pd.read_csv(file_to_plot, header=0, nrows=5, encoding='ISO-8859-1')  # for old DAV files 1997-2005

    print("_ number of columns: " + str(len(contents_head.columns)))
    contents_column_numbers = np.arange(0, len(contents_head.columns))

    chunks = [contents_column_numbers[x:x + 6] for x in range(0, len(contents_column_numbers), 6)]
    number_of_chunks = len(chunks)
    print("_ plotting " + str(chunks) + " parameters per page")
    print("_ pages needed to plot all " + str(len(contents_head.columns)) + " parameters: " + str(number_of_chunks))

    print("_ reading contents for plotting")

    for xx in range(0, number_of_chunks):

        print("\n\n_ now working on plotting chunk {0}/{1}".format(str(xx + 1), str(number_of_chunks)))
        print("_ ___ reading columns " + str(chunks[xx]) + " ...")

        # contents = pd.read_csv(file_to_plot, header=0, usecols=chunks[xx])

        try:
            contents = pd.read_csv(file_to_plot, header=0, usecols=chunks[xx])
        except:
            contents = pd.read_csv(file_to_plot, header=0, usecols=chunks[xx], encoding='ISO-8859-1')  # for old DAV files 1997-2005

        print("_ ___ ... successful!")

        # read columns names from file_to_plot, count columns in file_to_plot
        columns_names = contents.columns
        print("_ ___ found columns:")
        print(columns_names)
        columns_count = len(contents.columns)  # = number of plots!
        print("_ ___ number of columns: " + str(columns_count))

        # assemble PNG output
        fig = plt.figure(figsize=(20, 12), dpi=150)
        make_plot_tight = 1  # generally plt.tight_layout() is applied

        for scalar in range(0, columns_count):
            print("\n_ ___ working on: " + columns_names[scalar] + " (column #" + str(scalar + 1) + " of " + str(columns_count) + ")")
            y = contents[columns_names[scalar]]

            print("_ ___ values: " + str(len(y)))

            if y.dtypes != object:  # skip if data is not numeric
                y = y.replace(-9999, np.nan)  # general missing value
                y = y.replace(-6999, np.nan)  # special missing value used for some scalars, e.g. ch4
                y = y.dropna()

                if not y.empty:
                    label_size = 8

                    # time series plot
                    ax1 = plt.subplot2grid((columns_count, 4), (scalar, 0), colspan=4)  # , rowspan=2)

                    # max 500000 data points will be plotted
                    downsampled = 0
                    if len(y) > 500000:
                        step = len(y) / 500000
                        step = int(step)
                        # if step < 10:
                        #     step = 10
                        # step = 100
                        y = y[::step]
                        print("_ ___ data series downsampled for plotting, values for plotting: " + str(len(y)))
                        print("_ ___ plotting only every " + str(step) + "th data point.")
                        downsampled = 1

                    # plt.plot(y.index, y, '#5f87ae', markersize=0.01, alpha=0.9, marker='ro')
                    plt.plot(y.index, y, 'r,', alpha=0.5, c='#5f87ae')
                    # plt.scatter(y.index, y, c='#5f87ae', edgecolor='#5f87ae', s=0.1, alpha=0.8)
                    plt.xlim(min(y.index), max(y.index))
                    # qua1 = y.quantile(0.01)
                    # qua2 = y.quantile(0.99)
                    # plt.ylim(qua1, qua2)

                    # y.plot(c='#5f87ae', linewidth=0.2)  # blue: #5f87ae

                    if scalar < columns_count - 1:
                        ax1.tick_params(axis='x', which='both', labelbottom='off')
                    else:
                        ax1.tick_params(axis='x', which='both', labelbottom='on', labelsize=label_size)
                        ax1.set_xlabel("record number", size=label_size)

                    ax1.tick_params(axis='y', labelsize=label_size)
                    plt.setp(ax1.xaxis.get_majorticklabels(), rotation=0)
                    ax1.text(.05, .75, columns_names[scalar], horizontalalignment='right', transform=ax1.transAxes,
                             backgroundcolor='#CCCCCC', size=10, color='black')

                    if downsampled == 0:
                        ax1.text(0.80, .5, "values: " + str(len(y)) + " | mean: " + str("%.2f" % y.mean()) + "+/-" + str("%.2f" % y.std()) +
                                 "\nmedian: " + str("%.2f" % y.median()) + " | min: " + str("%.2f" % y.min()) + " | max: " + str("%.2f" % y.max()),
                                 horizontalalignment='left', transform=ax1.transAxes,
                                 size=12, color='black')
                    else:  # "%.2f" % a
                        ax1.text(0.80, .5, "(values: " + str(len(y)) + ") | mean: " + str("%.2f" % y.mean()) + "+/-" + str("%.2f" % y.std()) +
                                 "\nmedian: " + str("%.2f" % y.median()) + " | min: " + str("%.2f" % y.min()) + " | max: " + str("%.2f" % y.max()),
                                 horizontalalignment='left', transform=ax1.transAxes,
                                 size=12, color='black')
                        fig.text(0.8, 0.95, "DOWNSAMPLED", color='red', size=24)

                else:
                    print("Data for " + columns_names[scalar] + " is empty --> no plot")
                    make_plot_tight = 0  # 0 = no, 1 = yes; plt.tight_layout() causes error if a data series is empty

        fig.subplots_adjust(hspace=0.1)
        # fig.text(0, 0.92, "DOWNSAMPLED", color='red', size=24)
        if make_plot_tight == 1:  # only apply plt.tight_layout() if flag is set to 1, i.e. data series are not empty (would cause crash otherwise)
            plt.tight_layout()
        print("_ ___ saving PNG image ...")
        plot_name = os.path.join(destination_folder, plot_name)

        plt.savefig(plot_name + "_" + str(xx + 1) + '.png', dpi=90)
        plt.close()

        print("_ ___ finished plotting chunk {0}/{1}".format(str(xx + 1), str(number_of_chunks)))

        # # Agg rendering:
        # # 0 to disable; values in the range
        # # 10000 to 100000 can improve speed slightly
        # # and prevent an Agg rendering failure
        # # when plotting very large data sets,
        # # especially if they are very gappy.
        # mpl.rcParams['agg.path.chunksize'] = 100000
        # print("chunksize set to " + str(mpl.rcParams['agg.path.chunksize']))


def plot_full_output(file_to_plot, destination_folder):

    # small function to read date column properly
    parse = lambda x: dt.datetime.strptime(x, '%Y-%m-%d %H:%M')  # standard date format of the EddyPro full_output file

    file = file_to_plot

    # read data file, generates data frame (similar to R)
    contents = pd.read_csv(file, skiprows=[0, 2],
                           parse_dates=[['date', 'time']], index_col=0, date_parser=parse)
    # read start of data file to get units (in 3rd row)

    # more info: http: // pandas.pydata.org / pandas - docs / stable / io.html  # duplicate-names-parsing
    units = pd.read_csv(file, skiprows=[0, 1], nrows=1, mangle_dupe_cols=True)
    units = units.columns[2:]
    print(units)

    # look at what date range of 30min data we have
    first_date = contents.index[0]  # first entry
    last_date = contents.index[-1]  # last entry
    print("found first date: " + str(first_date))

    if last_date.day - first_date.day < 5:  # if we have less than 5 days of data add dates to avoid problems with results plotting
        last_date = last_date + pd.offsets.Day(20)
        print("found last date (extended by 20 days for plotting): " + str(last_date))
    else:
        print("found last date: " + str(last_date))

    # generate continuous date range and re-index data
    filled_date_range = pd.date_range(first_date, last_date, freq='30T')
    contents = contents.reindex(filled_date_range, fill_value=-9999)  # apply new continuous index to data

    plot_folder = os.path.join(destination_folder)

    # read columns names from file, count columns in file
    columns_names = contents.columns
    print(columns_names)
    columns_count = len(contents.columns)
    print("number of columns: " + str(columns_count))

    # assemble PNG output
    for scalar in range(2, columns_count):

        print("working on: " + columns_names[scalar] + " (column #" + str(scalar) + " of " + str(columns_count) + ")")
        plot_name = str(scalar) + "_" + columns_names[scalar]

        plot_name = plot_name.replace('*', 'star')
        plot_name = plot_name.replace('/', '_over_')

        y = contents[columns_names[scalar]]

        # if qc flag available, filter for quality 0 and 1 fluxes, do not use quality 2 fluxes
        qc_string = "qc_" + columns_names[scalar]

        quality_controlled = 0  # 0 = no, 1 = yes
        if qc_string in columns_names:
            qc = contents[qc_string] < 2  # qc = quality control flags, 0 = very good, 1 = OK
            y = y[qc]
            quality_controlled = 1

        fig = plt.figure(figsize=(11.69, 6.58), dpi=300)

        y = y.replace('Infinity', np.nan)  # replace 'Infinity' string (if found...)
        y = y.replace('-Infinity', np.nan)  # replace '-Infinity' string (if found...)
        y = y.replace('NaN', np.nan)  # should not be necessary... but is necessary sometimes!
        y = y.replace('#N/A', np.nan)  # should not be necessary...
        y = y.replace('#NV', np.nan)  # should not be necessary...
        y = y.astype(float)  # convert Series to NUMERIC
        y = y.replace(-9999, np.nan)  # general missing value
        y = y.replace(-6999, np.nan)  # special missing value used for some scalars, e.g. ch4
        y = y.dropna()  # remove all missing values
        # unique_values = np.unique(y)

        if not y.empty:  # and len(unique_values) > 1:

            heading_size = 8
            label_size = 7
            text_size = 8

            # fig = plt.subplot2grid((3, 3), (0, 0))

            qua1 = y.quantile(0.01)
            qua2 = y.quantile(0.99)

        # TIME SERIES PLOT
            ax1 = plt.subplot2grid((4, 4), (0, 0), colspan=4, rowspan=2)
            # y.plot(kind='scatter', x=y.index, y=y, ax=ax1)
            # y.plot(c='#5f87ae', linewidth=0.3)  # green=#bdd442
            # plt.scatter(y.index, y, c='#5f87ae', edgecolor='#5f87ae', alpha=1, s=1)
            # ax1.plot_date(y.index.to_pydatetime(), y, 'o-', color='#5f87ae', linewidth=0.2, markersize=3, markeredgecolor='#5f87ae')  # http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.plot_date
            ax1.plot_date(y.index.to_pydatetime(), y, '-', color='#5f87ae', linewidth=0.3)  # http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.plot_date
            plt.axhline(y.quantile(0.05), color='red', alpha=0.25)
            plt.axhline(y.quantile(0.95), color='red', alpha=0.25)
            ax1.set_xlabel("DAY/MONTH", size=label_size)
            ax1.set_ylabel(units[scalar], size=label_size)
            ax1.set_title(str(columns_names[scalar]) + " time series with 5/95 percentiles", size=heading_size, backgroundcolor='#5f87ae')
            ax1.tick_params(axis='both', labelsize=label_size)
            # plt.setp(ax1.xaxis.get_majorticklabels(), rotation=0)
            ax1.xaxis.set_major_formatter(dates.DateFormatter("%d/%m"))
            # ax1.xaxis.set_major_locator(dates.WeekdayLocator(byweekday=1, interval=1))
            plt.ylim(qua1, qua2)

        # DAILY AVG SCATTER
            ax4 = plt.subplot2grid((4, 4), (2, 0), colspan=2)
            daily_count = y.resample('D', how='count')
            daily_avg = y.resample('D', how='mean')
            daily_std = y.resample('D', how='std')
            # daily_avg = daily_avg[daily_count > 10]
            # daily_std = daily_std[daily_count > 10]
            plt.scatter(daily_avg.index.to_pydatetime(), daily_avg, c='#f78b31', edgecolor='#f78b31', alpha=1, s=4)
            plt.errorbar(daily_avg.index.to_pydatetime(), daily_avg, alpha=0.2, yerr=daily_std, capsize=0, ls='none', color='#f78b31', elinewidth=2)
            # plt.fill_between(daily_std.index, daily_avg - daily_std, daily_avg + daily_std, color='k', alpha=0.1)
            plt.axhline(0, color='grey', alpha=0.5)
            ax4.set_xlabel("DAY/MONTH", size=label_size)
            ax4.set_ylabel(units[scalar], size=label_size)
            ax4.set_title(str(columns_names[scalar]) + " daily average with std", size=heading_size, backgroundcolor='#f78b31')
            ax4.tick_params(axis='both', labelsize=label_size)
            plt.setp(ax4.xaxis.get_majorticklabels(), rotation=0)
            ax4.xaxis.set_major_formatter(dates.DateFormatter("%d/%m"))
            plt.ylim(qua1, qua2)
            # plt.ylim(-10, 10)

        # HISTOGRAM
            try:
                ax2 = plt.subplot2grid((4, 4), (3, 0), colspan=2)
                # y.hist(color='#5b9bd5', bins=10)
                # y_hist = y[y > qua1]
                plt.hist(y, bins=10, range=(qua1, qua2), color='#5b9bd5', edgecolor='#497CDD')
                # plt.xlim(qua1, qua2)
                ax2.set_xlabel(units[scalar], size=label_size)
                ax2.set_ylabel("counts", size=label_size)
                ax2.set_title(str(columns_names[scalar]) + " histogram", size=heading_size, backgroundcolor='#5b9bd5')
                ax2.tick_params(axis='both', labelsize=label_size)
            except ValueError as e:
                print("(!)Error during histogram generation: {}".format(e))
                pass

        # CUMULATIVE
            ax5 = plt.subplot2grid((4, 4), (2, 2), colspan=2)
            # y.cumsum().plot(c='#ed3744', linewidth=0.6)
            ax5.plot_date(y.index.to_pydatetime(), y.cumsum(), '-', color='#ed3744', linewidth=0.6)
            ax5.set_xlabel("day/month", size=label_size)
            ax5.set_ylabel(units[scalar], size=label_size)
            ax5.set_title(str(columns_names[scalar]) + " cumulative", size=heading_size, backgroundcolor='#ed3744')
            ax5.tick_params(axis='both', labelsize=label_size)
            plt.setp(ax5.xaxis.get_majorticklabels(), rotation=0)
            ax5.xaxis.set_major_formatter(dates.DateFormatter("%d/%m"))
            # ax5.xaxis.set_major_locator(dates.MonthLocator())
            ax5.yaxis.grid()
            plt.axhline(0, color='black', alpha=0.8)

        # # TIME SERIES MEDIAN PLOT w/ QUANTILES
        #     ax3 = plt.subplot2grid((4, 4), (3, 0), colspan=2)
        #     rmed = rolling_median(y, 50)  # rolling median value
        #     # rmed.plot(style='k')
        #     ax3.plot_date(rmed.index.to_pydatetime(), rmed, '-', color='#ed3744', linewidth=0.6)
        #     rqua75 = rolling_quantile(y, 50, 0.75)  # rolling quantile
        #     rqua25 = rolling_quantile(y, 50, 0.25)
        #     plt.fill_between(rqua75.index, rqua25, rqua75, color='b', alpha=0.2)
        #     ax3.set_xlabel("day/month", size=label_size)
        #     ax3.set_ylabel(units[scalar], size=label_size)
        #     ax3.set_title(str(columns_names[scalar]) + " running median (50 values) + running 25/75 percentile",
        #                   size=heading_size, backgroundcolor='#6e8588')
        #     ax3.tick_params(axis='both', labelsize=label_size)
        #     # plt.setp(ax3.xaxis.get_majorticklabels(), rotation=0)
        #     ax3.xaxis.set_major_formatter(dates.DateFormatter("%d/%m"))

        # HOURLY AVERAGE
            ax6 = plt.subplot2grid((4, 4), (3, 2), colspan=1, rowspan=1)
            hourly_avg = y.groupby(y.index.hour).mean()
            hourly_std = y.groupby(y.index.hour).std()
            hourly_avg.plot()
            plt.fill_between(hourly_avg.index, hourly_avg - hourly_std, hourly_avg + hourly_std, color='k', alpha=0.1)
            ax6.set_xlabel("hour", size=label_size)
            ax6.set_ylabel(units[scalar], size=label_size)
            ax6.set_title(str(columns_names[scalar]) + " diurnal cycle with std", size=heading_size, backgroundcolor='#ffc532')
            ax6.tick_params(axis='both', labelsize=label_size)
            plt.setp(ax6.xaxis.get_majorticklabels(), rotation=0)
            plt.axhline(0, color='black', alpha=0.8)
            plt.xlim(0, 23)
            if hourly_avg.min() < 0:
                factor = 1.2
            else:
                factor = 0.8
            plt.ylim(hourly_avg.min() * factor, hourly_avg.max() * 1.2)
            majorLocator = MultipleLocator(2)
            ax6.xaxis.set_major_locator(majorLocator)

        # TEXT INFO OUTPUT
            fig.text(0.8, 0.05, y.describe(), size=text_size, backgroundcolor='#CCCCCC')
            if quality_controlled == 1:
                plt.figtext(0.84, 0.97, "quality controlled using " + qc_string, color='red', size=text_size)

            # plt.show()
            plt.tight_layout()
            # fig.subplots_adjust(hspace=0.1)
            print("Saving plots to PNG image...")
            plot_name = os.path.join(plot_folder, plot_name)
            plt.savefig(plot_name + '.png', dpi=150)
            plt.close()

        else:
            print("Data for " + columns_names[scalar] + " is empty --> no plot")

    print("Finished full_output plots!")


def plot_diurnal_cycles(file_to_plot, destination_folder):

    # small function to read date column properly
    parse = lambda x: dt.datetime.strptime(x, '%Y-%m-%d %H:%M')

    file = file_to_plot  # long file, 1 yr, many vars
    # file = 'M:\\Dropbox\\luhk_work\\PR_python_projects\\ETH_FluxCalc_with_EddyPro\\ETH_FluxCalc_v3\\OUTPUT_LAE_2014-07-12T185149\\eddypro\\results\\eddypro_LAE_full_output_2014-07-12T185226.csv'

    # read data file, generates data frame (similar to R)
    contents = pd.read_csv(file, skiprows=[0, 2],
                           parse_dates=[['date', 'time']], index_col=0, date_parser=parse)
    # read start of data file to get units (in 3rd row)

    # http://pandas.pydata.org/pandas-docs/stable/io.html#duplicate-names-parsing
    units = pd.read_csv(file, skiprows=[0, 1], nrows=1, mangle_dupe_cols=True)
    units = units.columns[2:]
    print(units)

    # read columns names from file, count columns in file
    columns_names = contents.columns
    print(columns_names)
    columns_count = len(contents.columns)
    print(columns_count)

    # assemble PNG output
    for scalar in range(2, columns_count):
        print("working on: " + columns_names[scalar] + " (column #" + str(scalar) + " of " + str(columns_count) + ")")
        plot_name = str(scalar) + "_" + columns_names[scalar]

        plot_name = plot_name.replace('*', 'star')
        plot_name = plot_name.replace('/', '_over_')

        y = contents[columns_names[scalar]]

        # if qc flag available, filter for quality 0 and 1 fluxes, do not use quality 2 fluxes
        qc_string = "qc_" + columns_names[scalar]
        quality_controlled = 0  # 0 = no, 1 = yes
        if qc_string in columns_names:
            qc = contents[qc_string] < 2  # qc = quality control flags, 0 = very good, 1 = OK
            y = y[qc]
            quality_controlled = 1

        fig = plt.figure(figsize=(11.69, 6.58), dpi=150)

        plt.title("aaa")

        y = y.replace('Infinity', np.nan)  # replace 'Infinity' string (if found...)
        y = y.replace('-Infinity', np.nan)  # replace '-Infinity' string (if found...)
        y = y.replace('NaN', np.nan)  # should not be necessary...
        y = y.replace('#N/A', np.nan)  # should not be necessary...
        y = y.replace('#NV', np.nan)  # should not be necessary...
        y = y.astype(float)  # convert Series to NUMERIC
        y = y.replace(-9999, np.nan)  # general missing value
        y = y.replace(-6999, np.nan)  # special missing value used for some scalars, e.g. ch4
        y = y.dropna()  # remove all missing values

        if not y.empty:

            heading_size = 12
            subheading_size = 10
            label_size = 7
            text_size = 7

            # let's see which months we have in our time series
            y_found_months = y.index.month
            y_found_months = np.unique(y_found_months)

            ax = plt.subplot2grid((4, 3), (0, 0))

            # not let's cycle through all found months
            plot_counter = 0
            for month_cycler in range(0, len(y_found_months)):

                plot_counter += 1

                y_one_month = y[y.index.month == y_found_months[month_cycler]]  # loads y data only from current month

                if 1 <= month_cycler + 1 <= 3:
                    plot_row = 0
                    plot_column = month_cycler
                elif 4 <= month_cycler + 1 <= 6:
                    plot_row = 1
                    plot_column = month_cycler - 3
                elif 7 <= month_cycler + 1 <= 9:
                    plot_row = 2
                    plot_column = month_cycler - 6
                elif 10 <= month_cycler + 1 <= 12:
                    plot_row = 3
                    plot_column = month_cycler - 9
                else:
                    input("Month not valid. Please check file. Stopping program. Sorry.")
                    sys.exit()

                # hourly average
                ax1 = plt.subplot2grid((4, 3), (plot_row, plot_column), colspan=1, rowspan=1)
                hourly_avg = y_one_month.groupby(y_one_month.index.hour).mean()
                hourly_std = y_one_month.groupby(y_one_month.index.hour).std()
                hourly_avg.plot()
                plt.fill_between(hourly_avg.index, hourly_avg - hourly_std, hourly_avg + hourly_std, color='k',
                                 alpha=0.1)
                ax1.set_xlabel("hour", size=label_size)
                ax1.set_ylabel(units[scalar], size=label_size)
                ax1.text(0.06, 0.85, str(y_found_months[month_cycler]), horizontalalignment='center',
                         verticalalignment='center', transform=ax1.transAxes, backgroundcolor='#ffc532',
                         size=subheading_size)
                if plot_counter == 1:
                    ax1.text(0.05, 1.1, columns_names[scalar], horizontalalignment='left',
                             verticalalignment='baseline', transform=ax1.transAxes, backgroundcolor='#b1d32f',
                             size=heading_size, )
                ax1.tick_params(axis='both', labelsize=label_size)
                plt.setp(ax1.xaxis.get_majorticklabels(), rotation=0)
                plt.axhline(0, color='black', alpha=0.8)
                plt.xlim(0, 23)
                if hourly_avg.min() < 0:
                    factor = 1.2
                else:
                    factor = 0.8
                plt.ylim(hourly_avg.min() * factor, hourly_avg.max() * 1.2)
                majorLocator = MultipleLocator(2)
                ax1.xaxis.set_major_locator(majorLocator)

            # plt.show()
            # plt.tight_layout()
            # fig.subplots_adjust(hspace=0.1)
            print("Saving plots to PNG image...")
            plot_name = os.path.join(destination_folder, plot_name)
            plt.savefig(plot_name + '.png', dpi=150)
            plt.close()

        else:
            print("Data for " + columns_names[scalar] + " is empty --> no plot")

    print("Finished!")


    # # example: replace a number in a Series with NaN (indicates missing value)
    # test = contents['H']
    # print(test.head())
    # test2 = test.replace(-9999, np.nan)
    # print(test2.head())

    # # example: replace a number in a Series with another number
    # test = contents['H']
    # print(test.head())
    # test2 = test.replace(-9999, 5000)
    # print(test2.head())

    # # example: only use values > 0 in the Series
    # test = contents['H']
    # test2 = test[test > 0]
    # print(test.head())
    # print(test2.head())

    # # example: display ticks and respective values
    # outputs ticks & values
    # ticks = contents.ix[:, ['H']]
    # print(ticks.head())

    # # example: replace a number in a Series with NaN (indicates missing value)
    # test = contents['H']
    # print(test.head())
    # test2 = test.replace(-9999, np.nan)
    # print(test2.head())

    # # example: replace a number in a Series with another number
    # test = contents['H']
    # print(test.head())
    # test2 = test.replace(-9999, 5000)
    # print(test2.head())

    # # example: only use values > 0 in the Series
    # test = contents['H']
    # test2 = test[test > 0]
    # print(test.head())
    # print(test2.head())

    # # example: display ticks and respective values
    # outputs ticks & values
    # ticks = contents.ix[:, ['H']]
    # print(ticks.head())

    # # test:
    # file_to_plot = "201306120800.csv"
    # test_file_path = "E:\\Dropbox\\Dropbox\\luhk_work\\PR_python_projects\\convert_eth_binary_to_ascii\\output_LAE\\raw_data_csv\\201306120800.csv"
    # plot_converted_raw(file_to_plot,
    #                    "E:\\Dropbox\\Dropbox\\luhk_work\\PR_python_projects\\convert_eth_binary_to_ascii\\output_LAE\\raw_data_csv_plots")

    # # example: replace a number in a Series with NaN (indicates missing value)
    # test = contents['H']
    # print(test.head())
    # test2 = test.replace(-9999, np.nan)
    # print(test2.head())

    # # example: replace a number in a Series with another number
    # test = contents['H']
    # print(test.head())
    # test2 = test.replace(-9999, 5000)
    # print(test2.head())

    # # example: only use values > 0 in the Series
    # test = contents['H']
    # test2 = test[test > 0]
    # print(test.head())
    # print(test2.head())

    # # example: display ticks and respective values
    # outputs ticks & values
    # ticks = contents.ix[:, ['H']]
    # print(ticks.head())


def plot_raw_data_availability(source_folder, destination_folder, raw_file_extension, file_duration):

    files_per_day = int(file_duration) / 1440

    allfiles = os.listdir(source_folder)
    if raw_file_extension == '*.csv':
        required_filename_length = 16
    else:
        required_filename_length = 14

    # grid: 12 months x 31 days = 372 slots
    data_grid = np.zeros((12, 31), dtype=np.int)
    data_grid[1, 28:] = 0  # set February 29-31 to 0
    data_grid[3, 30] = 0  # set April 31 to 0
    data_grid[5, 30] = 0  # set June 31 to 0
    data_grid[8, 30] = 0  # set September 31 to 0
    data_grid[10, 30] = 0  # set November 31 to 0

    number_of_days_with_data = 0
    for found_file in allfiles:
        if fnmatch.fnmatch(found_file, raw_file_extension) and len(found_file) == required_filename_length and \
                found_file[0:4].isdigit() and found_file[4:6].isdigit() and \
                found_file[6:8].isdigit() and found_file[8:10].isdigit():

            filename = os.path.splitext(found_file)[0]
            year = int(filename[0:4])

            # month defines y
            month = int(filename[4:6])
            y_position = month - 1

            # day and hour define x
            day = int(filename[6:8])
            x_position = day - 1

            print("\n")
            print("File #" + str(number_of_days_with_data))
            print("Found file: " + filename)
            print("Found date: ")
            print("year: " + str(year))
            print("month: " + str(month))
            print("day: " + str(day))
            print("x: " + str(x_position))
            print("y: " + str(y_position))

            found_file_full_path = os.path.join(source_folder, found_file)
            if data_grid[y_position, x_position] == 0:
                data_grid[y_position, x_position] = os.stat(found_file_full_path).st_size / 1024  # convert from Bytes to KB
                number_of_days_with_data += 1
            elif data_grid[y_position, x_position] != 0:
                data_grid[y_position, x_position] += os.stat(found_file_full_path).st_size / 1024

    data_coverage = np.round((number_of_days_with_data / 365) * 100, decimals=1)  # in %

    # data_grid = np.flipud(data_grid)
    fig, ax = plt.subplots(figsize=(16, 4))

    # colormap = plt.matplotlib.colors.ListedColormap(['white', '#adc800', '#AAAAAA'])
    s = ax.pcolor(data_grid, edgecolors='#AAAAAA', linewidths=1, cmap='PuBu')
    plt.colorbar(s, format='%.0f')

    # plt.title(str(year) + ": data coverage " + str(data_coverage) + "%", fontsize=20)
    plt.title("Data Availability {} in KiloBytes\ndata coverage: {} days ({} %)".format(str(year), number_of_days_with_data, data_coverage), fontsize=14)

    # plt.suptitle("Data Availability in KiloBytes", fontsize=14)
    plt.xticks(np.arange(1, 32, 1), np.arange(1, 32, 1), fontsize=12)
    plt.yticks(np.arange(1, 13, 1), fontsize=12)
    ax.set_ylabel("month", fontsize=12)
    ax.set_xlabel("day", fontsize=12)

    minorLocator = MultipleLocator(4)
    ax.xaxis.set_minor_locator(minorLocator)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_horizontalalignment('right')

    for tick in ax.xaxis.get_minor_ticks():
        tick.tick1line.set_markersize(200)
        tick.tick2line.set_markersize(200)
        tick.label1.set_horizontalalignment('center')

    for tick in ax.yaxis.get_major_ticks():
        tick.tick1line.set_markersize(0)
        tick.tick2line.set_markersize(0)
        tick.label1.set_verticalalignment('top')

    plt.xlim(0, 31)
    plt.tight_layout()
    out_file = os.path.join(destination_folder, "available_data_" + str(year) + ".png")
    plt.savefig(out_file, dpi=90, bbox_inches="tight")
    # plt.show()


