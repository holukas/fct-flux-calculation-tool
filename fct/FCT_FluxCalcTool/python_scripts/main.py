
def main(root_dir, source_dir):

    # switch for skipping the gui, if True no gui window appears and settings are used
    # as stored in the settings.txt
    # necessary for DAV automatic calculations
    skip_gui = False

    import os
    import datetime as dt
    import csv
    import fnmatch
    import subprocess
    import shutil
    import sys
    import FCT_FluxCalcTool.info.version_info as version_info
    import FCT_FluxCalcTool.python_scripts.setup as setup
    import FCT_FluxCalcTool.python_scripts.data_blocks as data_blocks
    import FCT_FluxCalcTool.python_scripts.plot as plot
    import FCT_FluxCalcTool.python_scripts.func as func
    import FCT_FluxCalcTool.python_scripts.gui as gui
    from pathlib import Path

    version_info_dict = version_info.version_info()


    # test_output = 0  # if 1, then only first 50 rows in binary data are converted to ascii todo

    # class to write to console AND log file with print
    class Tee(object):
        def __init__(self, *files):
            self.files = files

        def write(self, obj):
            for ff in self.files:
                ff.write(obj)

        def flush(self):
            pass

    # os.chdir("..")
    # root_dir = os.getcwd()

    # generate now_string for identification
    now_string = func.generate_id_string()

    # write all console output to logfile txt file
    # logfile will be moved from local root folder to output folder once finished
    logfile_out = "ID" + now_string + "_is_currently_running.txt"
    logfile_out_full_path = os.path.join(root_dir, logfile_out)
    logfile_out_full_path = open(logfile_out_full_path, 'w')
    original = sys.stdout
    sys.stdout = Tee(sys.stdout, logfile_out_full_path)

    print("ID" + now_string)

    # show intro / splash screen
    func.intro_screen()

    print("\n------------------------------------------")
    print("--> running FluxCalc version    {}" + version_info_dict['FluxCalcTool'])
    print("--> running EddyPro version     {}" + version_info_dict['EddyPro'])
    print("--> last edit                   {}" + version_info_dict['last edit'])
    print("\n------------------------------------------")
    print("LOCAL ROOT FOLDER")
    print(root_dir)
    print("------------------------------------------")

    # start GUI and read settings from GUI
    settings = gui.settings(source_dir=source_dir, skip_gui=skip_gui)
    download_raw_binary_data_from_server = settings[0]
    process_how_many_files_max = settings[1]  # this setting also determines how many files are downloaded
    convert_raw = settings[2]
    download_eddypro_settings_from_server = 0  # todo currently not implemented
    # download_eddypro_settings_from_server = settings[3]  # todo currently not implemented
    plot_raw_binary_data_availability = settings[4]
    plot_raw = settings[5]
    calc_fluxes = settings[6]
    plot_full_output = settings[7]
    plot_diurnal_cycles = settings[8]
    use_rds = settings[9]
    fluxes_from = settings[10]
    fluxes_until = settings[11]
    raw_binary_file_extension = settings[12]
    raw_binary_min_filesize_bytes = settings[13]
    site_to_process = settings[14]
    server_raw_binary_source_dir = settings[15]

    # if the gui is skipped, calculate fluxes for last x days
    if skip_gui == True:
        flux_days = 1

        fluxes_from = dt.datetime.now().date() - dt.timedelta(days=flux_days)
        fluxes_until = dt.datetime.now().date()

        fluxes_from = fluxes_from.strftime('%Y-%m-%d %H:%M')  # v0.81; generate string, to make format same as in GUI
        fluxes_until = fluxes_until.strftime('%Y-%m-%d %H:%M')

        print("\n------------------------------------------")
        print("GUI skipped, calculating fluxes for last {} days.".format(flux_days))
        print("Calculating fluxes from {} until {}.".format(fluxes_from, fluxes_until))

    eddypro_flux_settings_dir = "using local folder"  # todo

    # read settings
    print("\n------------------------------------------")
    print("READING SETTINGS")
    print("Found the following settings:")
    print("     site_to_process = " + str(site_to_process))
    print("     raw_binary_file_extension = " + str(raw_binary_file_extension))

    # if we don't want to convert raw binary files to *.csv then we also do not need to download data from the server
    if convert_raw == 0 and download_raw_binary_data_from_server == 1:
        download_raw_binary_data_from_server = 0
        print("     *download_raw_binary_data_from_server was set to 0 because convert_raw is 0")

    print("     download_raw_binary_data_from_server = " + str(download_raw_binary_data_from_server))
    print("     download_how_many_files_max = " + str(process_how_many_files_max))
    print("     download_eddypro_settings_from_server = " + str(download_eddypro_settings_from_server))
    print("     raw_binary_source_dir = " + str(server_raw_binary_source_dir))
    print("     eddypro_flux_settings_dir = " + str(eddypro_flux_settings_dir))
    print("     raw_binary_min_filesize_bytes = " + str(raw_binary_min_filesize_bytes))
    print("     fluxes_from = " + fluxes_from)
    print("     fluxes_until = " + fluxes_until)
    print("     plot_raw_binary_data_availability = " + str(plot_raw_binary_data_availability))
    print("     convert_raw = " + str(convert_raw))
    print("     plot_raw = " + str(plot_raw))
    print("     calc_fluxes = " + str(calc_fluxes))
    print("     plot_full_output = " + str(plot_full_output))
    print("     plot_diurnal_cycles = " + str(plot_diurnal_cycles))
    # print("\n Other defined source dirs in settings.txt:")
    # print("     raw_binary_source_dir = " + str(server_raw_binary_source_dir))
    print("     use_rds = " + str(use_rds))

    # get date info from first and last month option

    # v 0.80
    fluxes_from_datetime = dt.datetime.strptime(fluxes_from, '%Y-%m-%d %H:%M')
    fluxes_until_datetime = dt.datetime.strptime(fluxes_until, '%Y-%m-%d %H:%M')

    print("\nChecking validity of start date (fluxes_from in settings.txt)...")
    if fluxes_until_datetime < fluxes_from_datetime:
        print("\n!ERROR!: TIME CONFLICT, FLUXES_UNTIL MUST BE LATER THAN FLUXES_FROM.")
        print("STOPPING PROGRAM. PLEASE CHECK SETTINGS. SORRY.")
        sys.exit()
    else:
        print("...done! Looks fine.")

    # # todo now let's check if we have to download the EddyPro settings from the server
    # # if yes (=1) then go to server and search for the settings in the subfolder "eddypro_flux_settings"
    # # if no (=0), use the local settings files in the local root folder
    # if int(download_eddypro_settings_from_server) == 1:
    #     # if we download settings files from server, we first delete possible local settings files to avoid confusion
    #     print("\nEddyPro settings will be retrieved from server, deleting local EddyPro setting files...")
    #     allfiles = os.listdir(root_dir)
    #     for file in allfiles:
    #         if fnmatch.fnmatch(file, '*.eddypro'):  # if the file is the *.eddypro processing file
    #             os.remove(file)
    #             print("deleted file: " + file)
    #         elif fnmatch.fnmatch(file, '*.metadata'):  # if the file is the *.metadata file
    #             os.remove(file)
    #             print("deleted file: " + file)
    #
    #     # # data on server will be downloaded to local raw binary folder
    #     # print("\nConnecting to server to search for EddyPro settings...")
    #     # print("...ON SERVER | searching for folder \"eddypro_flux_settings\" in:")
    #     # print("......" + eddypro_flux_settings_dir)
    #     # allfiles = os.listdir(eddypro_flux_settings_dir)
    #
    #     # # this is the folder that must exist in the raw binary source folder, contains eddypro settings:
    #     # folder_eddypro_flux_settings = os.path.join(server_raw_binary_source_dir, "eddypro_flux_settings")
    #
    #     # for found_entry in allfiles:
    #     # if os.path.isdir(os.path.join(server_raw_binary_source_dir, found_entry)) and "eddypro_flux_settings" in found_entry:
    #     #     folder_eddypro_flux_settings = os.path.join(server_raw_binary_source_dir, "eddypro_flux_settings")
    #     #     print("......folder " + folder_eddypro_flux_settings + " found!")
    #     print("\n......searching for EddyPro settings in folder: " + eddypro_flux_settings_dir)
    #     for found_file in allfiles:
    #         if fnmatch.fnmatch(found_file, "*.eddypro"):
    #             print(".....found EddyPro processing file: " + found_file)
    #             print("......[+] downloading file: " + found_file)
    #             src = os.path.join(eddypro_flux_settings_dir, found_file)
    #             dest = os.path.join(root_dir, found_file)
    #             shutil.copy(src, dest)
    #
    #         if fnmatch.fnmatch(found_file, "*.metadata"):
    #             print("\n.....found EddyPro metadata file: " + found_file)
    #             print("......[+] downloading file: " + found_file)
    #             src = os.path.join(eddypro_flux_settings_dir, found_file)
    #             dest = os.path.join(root_dir, found_file)
    #             shutil.copy(src, dest)
    # else:
    #     print("\nUsing local EddyPro setting files, no download from server.")

    # get site name & instruments from EddyPro settings files
    print("\n------------------------------------------")
    print("\nDETECTING INSTRUMENTS from EddyPro settings files")
    detect_instruments = setup.detect_instruments(root_dir=root_dir)
    site_name = detect_instruments[0]
    data_block_order = detect_instruments[1]
    data_block_order_id = detect_instruments[2]
    file_duration = detect_instruments[3]
    for x in range(0, len(data_block_order)):
        if 'csat3' in data_block_order[x]:
            size_header = 38
            break
        else:
            size_header = 29

    # construct the header for the CSV file
    construct_header = setup.construct_header(data_block_order, data_block_order_id)
    header_csv = construct_header
    print("\nHeader for CSV Output:")
    print(header_csv)

    # setup folder structure
    print("\n------------------------------------------")
    print("\nSETTING UP FOLDER STRUCTURE")
    get_folder = setup.folder_structure(site_name, convert_raw, calc_fluxes, root_dir, source_dir,
                                        fluxes_from, fluxes_until, download_raw_binary_data_from_server, now_string,
                                        fluxes_from_datetime, fluxes_until_datetime)
    local_raw_csv_folder = get_folder[0]
    local_info_folder = get_folder[1]
    local_eddypro_bin_folder = get_folder[2]
    local_eddypro_ini_folder = get_folder[3]
    local_output_folder = get_folder[4]
    local_eddypro_results_folder = get_folder[5]
    local_raw_binary_folder = get_folder[6]
    local_raw_csv_plots_folder = get_folder[7]
    local_full_output_plots_folder = get_folder[8]
    local_diurnal_cycle_plots_folder = get_folder[9]
    local_data_availability_plots_folder = get_folder[10]
    site_now_string = get_folder[11]

    print("\nlocal output folder: " + local_output_folder)

    # raw binary data on server will be downloaded to local raw binary folder
    number_of_downloaded_files = 0
    if int(download_raw_binary_data_from_server) == 1:

        # # before download, empty local raw binary folder
        # filelist = os.listdir(local_raw_binary_folder)
        # for fileName in filelist:
        #     os.remove(local_raw_binary_folder + "/" + fileName)

        print("\n------------------------------------------")
        print("\nDOWNLOADING RAW FROM SERVER")
        print("\nEddyPro raw binary files will now be downloaded from the server...")
        print("Files will be downloaded to " + local_raw_binary_folder)

        # data on server will be downloaded to local raw binary folder
        print("\nConnecting to server to search for raw binary files...")

        print("...ON SERVER | searching for raw binary files in:")
        print("......" + server_raw_binary_source_dir)

        number_of_downloaded_files = 0
        ignore_subfolders = ['PROBLEM', 'PROBLEMS', 'QCL', 'WRONGDATE']
        print("......ignoring all subfolders named:")
        print(*ignore_subfolders, sep='\n')

        for root, dirs, files in os.walk(server_raw_binary_source_dir):

            if number_of_downloaded_files == process_how_many_files_max:
                print("\n------------------------")
                print("Maximum number of files to download reached. Stopping raw binary file search.")
                print("... Number of downloaded files: " + str(number_of_downloaded_files))
                print("... Maximum (from settings): " + str(process_how_many_files_max))
                print("------------------------")
                break

            # removing unwanted subfolders from list
            for ignore in ignore_subfolders:
                if ignore in dirs:
                    dirs.remove(ignore)
                    print("Ignoring subfolder " + ignore + "...")

            current_walk_directory = root.split('\\')
            current_walk_directory = current_walk_directory[-1]
            print("\nFound directory:")
            print(current_walk_directory)

            for found_file in files:

                found_file_full_path = os.path.realpath(os.path.join(root, found_file))
                print("......found file: " + found_file_full_path)

                if fnmatch.fnmatch(found_file, raw_binary_file_extension) and (
                        number_of_downloaded_files + 1) <= process_how_many_files_max:

                    if found_file[0:4].isdigit() and found_file[4:6].isdigit() and \
                            found_file[6:8].isdigit() and found_file[8:10].isdigit() and len(
                        found_file) == 14:  # check if date numbers from filename are numeric and have correct length
                        # check year and month of file from filename
                        this_year = int(found_file[0:4])
                        this_month = int(found_file[4:6])
                        this_day = int(found_file[6:8])
                        this_hour = int(found_file[8:10])
                        this_minute = int(found_file[13:])

                        if 1 <= this_month <= 12 and 1 <= this_day <= 31 and 0 <= this_hour <= 23 and 0 <= this_minute <= 59:
                            this_datetime = dt.datetime(this_year, this_month, this_day, this_hour, this_minute)

                            # download only "wanted" binary files as specified in the settings file with fluxes_from and fluxes_until
                            if fluxes_from_datetime <= this_datetime <= fluxes_until_datetime:
                                src = os.path.join(root, found_file)
                                info = os.stat(src)
                                print("......filesize: " + str(info.st_size) + " Bytes")
                                if info.st_size >= int(raw_binary_min_filesize_bytes):
                                    print("......[+] file size OK, downloading file...")
                                    dest = os.path.join(local_raw_binary_folder, found_file)
                                    shutil.copy(src, dest)
                                    number_of_downloaded_files += 1
                                else:
                                    print("......[-] file size too small, skipping file")
                            else:
                                print(
                                    "......[-] file outside specified time period (see fluxes_from and fluxes_until in settings), skipping file")
                        else:
                            print("......[-] date numbers of file are corrupted, e.g. month < 1; skipping file")
                    else:
                        print("......[-] date numbers from filename contain non-numeric characters, skipping file")
                else:
                    print(
                        "......[-] file is not raw binary data or max number of files to download reached (see download_how_many_files in settings), skipping file")

    else:
        print("\nNo files will be downloaded from server.")
        if convert_raw == 0:
            print("Using previously converted raw CSV files from folder " + local_raw_csv_folder)
        else:
            print(
                "\nUsing local raw binary files from folder " + local_raw_binary_folder + ", no download from server.")

    if number_of_downloaded_files == 0 and convert_raw == 1:
        print("\n\nNumber of files downloaded from server: no files were downloaded")
        # before we proceed, we have to check if there are raw binary data files in the local raw binary folder

        # before download, empty local raw binary folder
        filelist = os.listdir(local_raw_binary_folder)
        if len(filelist) == 0:
            print("The local raw binary folder:")
            print(local_raw_binary_folder)
            print("is empty! Stopping program. Sorry.")
            sys.exit()
        else:
            print("Found files in the local raw binary folder:")
            print(local_raw_binary_folder)
            print("File list:")
            print(filelist)
            print("Found number of files: " + str(len(filelist)))
    else:
        print("\n\nNumber of files downloaded from server: " + str(number_of_downloaded_files))

    # ----------------------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------

    print("\n------------------------------------------")
    print("\nPLOTTING DATA AVAILABILITY")
    if plot_raw_binary_data_availability == 1 and file_duration != -9999:
        if convert_raw == 1:
            print(
                "Data availability plots are generated from raw BINARY files in folder " + local_raw_binary_folder + ".")
            plot.plot_raw_data_availability(source_folder=local_raw_binary_folder,
                                            destination_folder=local_data_availability_plots_folder,
                                            raw_file_extension=raw_binary_file_extension, file_duration=file_duration)
        else:
            print("Data availability plots are generated from raw CSV files in folder " + local_raw_csv_folder + ".")
            plot.plot_raw_data_availability(source_folder=local_raw_csv_folder,
                                            destination_folder=local_data_availability_plots_folder,
                                            raw_file_extension='*.csv', file_duration=file_duration)
            # plot.plot_raw_data_availability(local_raw_csv_folder, local_data_availability_plots_folder, '*.csv')  # OLD
    else:
        print("Skipped. Data availability not plotted because not selected in settings.")

    # ----------------------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------
    # output header info used in this session
    # go to info folder

    print("\n------------------------------------------")
    print("\nOUTPUT: header info")
    os.chdir(local_info_folder)
    print("output header info to " + local_info_folder)
    with open('HEADER_INFO.csv', "w", newline='') as the_file:
        csv.register_dialect("custom", delimiter=" ", skipinitialspace=True)
        writer = csv.writer(the_file, dialect="custom")
        for tup in header_csv:
            writer.writerow(tup)

    # go to raw folder
    os.chdir(local_raw_binary_folder)

    allfiles = os.listdir(local_raw_binary_folder)
    converted_files_counter = 0  # outputs number of current file that is converted
    number_of_allfiles = len(allfiles)

    print("checking how many raw binary files are available ...")
    print("total number of raw binary files available: " + str(len(allfiles)))
    print("\n")

    # ----------------------------------------------------------------------------------------------------------
    # MAIN LOOP
    # open binary files
    if convert_raw == 1:
        print("\n------------------------------------------")
        print("\nCONVERTING RAW BINARY FILES TO ASCII CSV")
        for binary_filename in allfiles:

            print(binary_filename)

            # check if proper format
            if fnmatch.fnmatchcase(binary_filename, raw_binary_file_extension) and len(binary_filename) == 14 and \
                    binary_filename[0:4].isdigit() and binary_filename[4:6].isdigit() and \
                    binary_filename[6:8].isdigit() and binary_filename[8:10].isdigit() and \
                    binary_filename[13:].isdigit() and os.stat(binary_filename).st_size >= int(
                raw_binary_min_filesize_bytes):

                # counter += 1

                # check year and month of file from filename
                this_year = int(binary_filename[0:4])
                this_month = int(binary_filename[4:6])
                this_day = int(binary_filename[6:8])
                this_hour = int(binary_filename[8:10])
                this_minute = int(binary_filename[12:])

                if 1 <= this_month <= 12 and 1 <= this_day <= 31 and 0 <= this_hour <= 23 and 0 <= this_minute <= 59:
                    this_datetime = dt.datetime(this_year, this_month, this_day, this_hour, this_minute)
                else:
                    print("Month or day not valid. Skipping flux calculation for this file.")
                    this_datetime = dt.datetime(9999, 12, 31)

                # calculate fluxes only with "wanted" files as specified in the settings file with fluxes_from and fluxes_until
                # if fluxes_from_year <= this_year <= fluxes_until_year and fluxes_from_month <= this_month <= fluxes_until_month:
                if fluxes_from_datetime <= this_datetime <= fluxes_until_datetime and converted_files_counter + 1 <= process_how_many_files_max:

                    converted_files_counter += 1

                    csv_filename = os.path.splitext(binary_filename)[0]

                    csv_filename = csv_filename + str(this_minute).zfill(
                        2) + '.csv'  # "minutes" are found in the extension of the original raw binary file
                    print("\n")
                    print("------------------------FILE: " + binary_filename)
                    print("converting file #" + str(converted_files_counter) + " of " + str(number_of_allfiles))
                    print(binary_filename + ' is converted to --> ' + csv_filename)
                    print("Time information read from filename: " + str(this_datetime))

                    # make full path to file
                    csv_filename = os.path.join(local_raw_csv_folder, csv_filename)
                    binary_filename = os.path.join(local_raw_binary_folder, binary_filename)

                    with open(csv_filename, 'w', newline='') as open_csv:
                        spamWriter = csv.writer(open_csv, delimiter=',')
                        spamWriter.writerow(header_csv)

                        # now we start to read the raw binary data from the binary file
                        # running_counter = 0  # for checking only

                        # import struct
                        # with open(binary_filename, 'rb') as open_binary:
                        #     bytes = open_binary.read()
                        # # byte = bytes.read(29)
                        # s = struct.Struct('<c B c L B B B B B B B B B B B B B B B B B B L')
                        # unpacked_data = s.unpack(bytes[0:29])
                        # s = struct.Struct('>h h h h h h')  # big endian short integer
                        # # Big-endian systems store the most significant byte of a word in the smallest address
                        # unpacked_data = s.unpack(bytes[29:29 + 12])

                        with open(binary_filename, 'rb') as open_binary:
                            read_header = data_blocks.data_block_header(open_binary,
                                                                        size_header)  # first read header...
                            while 1:  # ...then loop through rest of binary file contents
                                read_data = ()  # starting a new line
                                for x in range(0, len(data_block_order)):
                                    new_data = getattr(data_blocks, data_block_order[x])(
                                        open_binary)  # calling function from string
                                    if new_data is None:
                                        break  # breaks out of FOR loop
                                    read_data += new_data  # if data are available --> add to line for output

                                if new_data is not None:
                                    data = read_data
                                    spamWriter.writerow(data)
                                else:
                                    break  # breaks out of WHILE loop
                        open_binary.close()
                        open_csv.close()

                        if int(download_raw_binary_data_from_server) == 1:
                            os.remove(binary_filename)  # delete local raw binary file after conversion
                            print(
                                "\nSERVER raw binary file " + binary_filename + " was downloaded to LOCAL and converted.")
                            print("LOCAL raw binary file " + binary_filename + " was deleted after conversion.")
                            print(
                                "--> Automatically downloaded raw binary files are deleted from LOCAL after conversion.")
                        else:
                            print("LOCAL raw binary file " + binary_filename + " was found in LOCAL and converted.")
                            print("LOCAL raw binary file " + binary_filename + " was not deleted after conversion.")
                            print("--> If LOCAL raw binary files are used they remain in LOCAL after conversion.")
                            print("--> Raw binary files already present in LOCAL are not deleted after conversion.")

                elif converted_files_counter + 1 > process_how_many_files_max:
                    print("\n* Skipping " + binary_filename + " because max number of files reached.")

                else:
                    print("\n* Skipping " + binary_filename + " because outside defined time range.")

            else:
                print(binary_filename + " not raw binary file or file not large enough, conversion skipped.")
    else:
        print("Skipped. No conversion. Converted raw files from a previous run are used, folder:")
        print(local_raw_csv_folder)

    # ----------------------------------------------------------------------------------------------------------
    print("\n------------------------------------------")
    print("\nPLOTTING RAW DATA CSV")
    if plot_raw == 1:
        os.chdir(local_raw_csv_folder)
        allfiles = os.listdir(local_raw_csv_folder)

        for raw_csv_filename in allfiles:
            if fnmatch.fnmatch(raw_csv_filename, '*.csv'):
                plot.plot_converted_raw(raw_csv_filename, local_raw_csv_plots_folder, use_rds)
    else:
        print("Skipped. No plotting.")

    # ----------------------------------------------------------------------------------------------------------
    if calc_fluxes == 1:
        # call eddypro and calculate fluxes
        print("\n------------------------------------------")
        print("\nCALCULATING FLUXES")

        # EDDYPRO RP
        # STEP 1 in flux processing: call eddypro_rp.exe, store its output in console and therefore to log file
        os.chdir(local_eddypro_bin_folder)  # go to eddypro bin folder
        cmd = "eddypro_rp.exe"  # execute exe todo for linux and osx
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE)  # call cmd command

        # Grab stdout line by line as it becomes available.  This will loop until process terminates.
        while process.poll() is None:
            line = process.stdout.readline().decode('utf-8').replace('\r\n',
                                                                     '')  # This blocks until it receives a newline.
            if 'processing new flux averaging period' in line:
                print(
                    "-------------------------------------------------------")  # to better see the different days in the output
            else:
                print(line)

        print(process.stdout.read().decode('utf-8').replace('\r\n',
                                                            ''))  # When the subprocess terminates there might be unconsumed output that still needs to be processed.
        process_status = process.wait()  # Wait for cmd to terminate. Get return returncode

        # CHECK IF RP FINISHED SUCCESSFULLY
        print("\n*************************")
        print("EddyPro RP return code : ", process_status)
        if process_status == 0:
            print(" eddypro_rp.exe finished successfully.")
        else:
            print(" (!) eddypro_rp.exe encountered a problem.")
        print("*************************\n")

        # CHECK IF FULL_OUTPUT WAS GENERATED
        found_rp = func.check_if_file_in_folder(search_file='*_full_output_*.csv', folder=local_eddypro_results_folder)
        if found_rp == True:
            skip_fcc = True
            print("(!)  EddyPro RP already generated a full_output file. FCC will be skipped.")
        else:
            skip_fcc = False

        # EDDYPRO FCC
        # STEP 2 in flux processing: eddypro_fcc.exe, only needed when rp was OK and generated NO full_output file
        if process_status == 0 and skip_fcc == False:

            os.chdir(local_eddypro_bin_folder)  # execute exe
            cmd = "eddypro_fcc.exe"  # execute exe todo for linux and osx
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE)

            # Grab stdout line by line as it becomes available.  This will loop until process terminates.
            while process.poll() is None:
                line = process.stdout.readline().decode('utf-8').replace('\r\n',
                                                                         '')  # This blocks until it receives a newline.
                print(line)

            print(process.stdout.read().decode('utf-8').replace('\r\n',
                                                                ''))  # When the subprocess terminates there might be unconsumed output that still needs to be processed.
            process = process.wait()  # Wait for cmd to terminate. Get return returncode

            # CHECK IF FCC FINISHED SUCCESSFULLY
            print("\n*************************")
            print("EddyPro FCC return code : ", process)
            if process_status == 0:
                print(" eddypro_fcc.exe finished successfully.")
            else:
                print(" (!) eddypro_fcc.exe encountered a problem.")
            print("*************************\n")

    else:
        print("No fluxes calculated.")

    # ----------------------------------------------------------------------------------------------------------
    # todo (de)activate
    print("\n------------------------------------------")
    print("\nPLOTTING OVERVIEW OF EDDYPRO full_output FILE")
    if plot_full_output == 1:
        allfiles = os.listdir(local_eddypro_results_folder)

        for filename in allfiles:
            if "_full_output_" in filename:
                filename = os.path.join(local_eddypro_results_folder, filename)
                print("... plotting file " + filename)
                plot.plot_full_output(filename, local_full_output_plots_folder)
    else:
        print("Skipped. Results not plotted.")

    # ----------------------------------------------------------------------------------------------------------
    # todo (de)activate
    print("\n------------------------------------------")
    print("\n[PLOTTING]")
    print("PLOTTING DIURNAL CYCLES FROM EDDYPRO full_output FILE")
    if plot_diurnal_cycles == 1:
        allfiles = os.listdir(local_eddypro_results_folder)

        for filename in allfiles:
            if "_full_output_" in filename:
                filename = os.path.join(local_eddypro_results_folder, filename)
                print("... plotting file " + filename)
                plot.plot_diurnal_cycles(filename, local_diurnal_cycle_plots_folder)
    else:
        print("Skipped. Diurnal cycles not plotted.")

    # # ----------------------------------------------------------------------------------------------------------
    # # todo (de)activate
    # # For plotting of specific file v 0.91
    # if plot_full_output == 1:
    #     search_here = \
    #         Path("E:\Dropbox\luhk_work\Programming\In Action\FCT_FluxCalcTool\FCT092_EP704\plot_this")
    #     allfiles = os.listdir(search_here)
    #
    #     for filename in allfiles:
    #         if "_full_output_" in filename:
    #             filename = Path(search_here) / Path(filename)
    #             print("... plotting file {}".format(filename))
    #             plot.plot_full_output(filename, local_full_output_plots_folder)
    #             plot.plot_diurnal_cycles(filename, local_diurnal_cycle_plots_folder)



    print("\n------------------------------------------")
    print("------------------------------------------")
    print("END OF PROGRAM.")

    # moving logfile to final destination (output folder)
    logfile_out_full_path.close()
    src = os.path.join(root_dir, logfile_out)
    dest = os.path.join(local_output_folder, "logfile_" + site_now_string + ".txt")
    # shutil.move(src, dest)
    shutil.copy(src, dest)

    if use_rds == 1:
        rds_all_logs_folder = r'P:\Flux\RDS_calculations\_latest_versions\_\logs'
        dest = os.path.join(rds_all_logs_folder, "logfile_" + site_now_string + ".txt")
        shutil.copy(src, dest)

    os.remove(src)  # delete logfile in root

    # print("Logfile available: " + logfile_out)

    # # TEST CONVERSION: must be 87.5 %
    # binary = '01111101'
    # lower_nibble = binary[4:]
    # decimal = int(str(lower_nibble), 2)
    # decimal = (decimal * 6.25) + 6.25
    # print(decimal)
