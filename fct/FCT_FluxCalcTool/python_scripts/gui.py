import os
import sys


def settings(source_dir, skip_gui):

    import tkinter

    master = tkinter.Tk()

    # initialize variables
    download_raw_binary_data_from_server = tkinter.IntVar()
    download_how_many_files_max = tkinter.IntVar()
    convert_raw = tkinter.IntVar()
    download_eddypro_settings_from_server = tkinter.IntVar()
    plot_availability = tkinter.IntVar()
    plot_raw = tkinter.IntVar()
    calc_fluxes = tkinter.IntVar()
    plot_full_output = tkinter.IntVar()
    plot_diurnal_cycles = tkinter.IntVar()
    use_rds = tkinter.IntVar()
    fluxes_from = tkinter.StringVar()
    fluxes_until = tkinter.StringVar()
    raw_binary_file_extension = tkinter.StringVar()
    raw_binary_min_filesize_bytes = tkinter.IntVar()
    site_to_process = tkinter.StringVar()
    server_raw_binary_source_dir = tkinter.StringVar()

    # site_to_process.trace("w", callback_site_changed)  # You can use the trace method to attach “observer” callbacks to the variable. The callback is called whenever the contents change.

    # read start values from settings file
    settings_in = "settings.txt"
    settings_in_full_path = os.path.join(source_dir, settings_in)
    with open(settings_in_full_path) as input_file:
        for line in input_file:
            if line.startswith('download_raw_binary_data_from_server'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                download_raw_binary_data_from_server.set(p_value)
            elif line.startswith('download_how_many_files_max'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                download_how_many_files_max.set(p_value)
            elif line.startswith('convert_raw'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                convert_raw.set(p_value)
            elif line.startswith('download_eddypro_settings_from_server'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                download_eddypro_settings_from_server.set(p_value)
            elif line.startswith('plot_availability'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                plot_availability.set(p_value)
            elif line.startswith('plot_raw'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                plot_raw.set(p_value)
            elif line.startswith('calc_fluxes'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                calc_fluxes.set(p_value)
            elif line.startswith('plot_full_output'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                plot_full_output.set(p_value)
            elif line.startswith('plot_diurnal_cycles'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                plot_diurnal_cycles.set(p_value)
            elif line.startswith('use_rds'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                use_rds.set(p_value)
            elif line.startswith('fluxes_from'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                fluxes_from.set(p_value)
            elif line.startswith('fluxes_until'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                fluxes_until.set(p_value)
            elif line.startswith('raw_binary_file_extension'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                raw_binary_file_extension.set(p_value)
            elif line.startswith('raw_binary_min_filesize_bytes'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                raw_binary_min_filesize_bytes.set(p_value)
            elif line.startswith('site_to_process'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                site_to_process.set(p_value)
            elif line.startswith('server_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                server_raw_binary_source_dir.set(p_value)
            elif line.startswith('AWS_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                AWS_raw_binary_source_dir = p_value
            elif line.startswith('CHA_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                CHA_raw_binary_source_dir = p_value
            elif line.startswith('DAV_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                DAV_raw_binary_source_dir = p_value
            elif line.startswith('FRU_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                FRU_raw_binary_source_dir = p_value
            elif line.startswith('INO_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                INO_raw_binary_source_dir = p_value
            elif line.startswith('LAE_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                LAE_raw_binary_source_dir = p_value
            elif line.startswith('LAS_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                LAS_raw_binary_source_dir = p_value
            elif line.startswith('OE2_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                OE2_raw_binary_source_dir = p_value
            elif line.startswith('TWE_raw_binary_source_dir'):  # indicates instrument information
                p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                TWE_raw_binary_source_dir = p_value

    master.wm_title("ETH FluxCalc using EddyPro")
    master.geometry('600x550+200+100')

    siteList = ('CH-AWS', 'CH-CHA', 'CH-DAV', 'CH-FRU', 'CH-INO', 'CH-LAE', 'CH-LAS', 'CH-OE2', 'US-TWE')
    # site_to_process.set(siteList[0])

    tkinter.Label(master, justify=tkinter.RIGHT, text="CHOOSE SITE").grid(column=0, row=0, sticky=tkinter.W)
    tkinter.OptionMenu(master, site_to_process, *siteList).grid(column=1, row=0, sticky=tkinter.W)

    tkinter.Label(master, text="// RAW DATA SETTINGS").grid(column=0, row=1, sticky=tkinter.W)
    tkinter.Checkbutton(master, text="download raw binary data from server", variable=download_raw_binary_data_from_server).grid(column=1, row=1, sticky=tkinter.W)

    tkinter.Label(master, text="max number of raw binary files to download").grid(column=1, row=2, sticky=tkinter.W)
    tkinter.Entry(master, textvariable=download_how_many_files_max).grid(column=2, row=2, sticky=tkinter.W)
    tkinter.Label(master, text="file extension of raw binary files").grid(column=1, row=3, sticky=tkinter.W)
    tkinter.Entry(master, textvariable=raw_binary_file_extension).grid(column=2, row=3, sticky=tkinter.W)
    tkinter.Label(master, text="min filesize of (Bytes)").grid(column=1, row=4, sticky=tkinter.W)
    tkinter.Entry(master, textvariable=raw_binary_min_filesize_bytes).grid(column=2, row=4, sticky=tkinter.W)

    tkinter.Checkbutton(master, text="convert raw binary files to *.csv", variable=convert_raw).grid(column=1, row=5, sticky=tkinter.W)

    fileformat_list = ('6-hour files', '1-hour files')
    tkinter.Checkbutton(master, text="plot raw data availability", variable=plot_availability).grid(column=1, row=6, sticky=tkinter.W)

    tkinter.Checkbutton(master, text="plot raw data", variable=plot_raw).grid(column=1, row=7, sticky=tkinter.W)
    tkinter.Label(master, text="- - -", justify=tkinter.RIGHT).grid(column=0, row=8, sticky=tkinter.W)

    tkinter.Label(master, text="// FLUX SETTINGS", justify=tkinter.RIGHT).grid(column=0, row=9, sticky=tkinter.W)
    tkinter.Checkbutton(master, text="calculate fluxes", variable=calc_fluxes).grid(column=1, row=9, sticky=tkinter.W)
    tkinter.Checkbutton(master, text="download EddyPro settings from server (not yet implemented)", variable=download_eddypro_settings_from_server).grid(column=1, row=10, sticky=tkinter.W)

    tkinter.Checkbutton(master, text="plot full output", variable=plot_full_output).grid(column=1, row=11, sticky=tkinter.W)
    tkinter.Checkbutton(master, text="plot diurnal cycles", variable=plot_diurnal_cycles).grid(column=1, row=12, sticky=tkinter.W)

    tkinter.Label(master, text="calculate fluxes from (YYYY-MM-DD hh:mm): ").grid(column=1, row=13, sticky=tkinter.W)
    tkinter.Entry(master, textvariable=fluxes_from).grid(column=2, row=13, sticky=tkinter.W)
    tkinter.Label(master, text="calculate fluxes until (YYYY-MM-DD hh:mm): ").grid(column=1, row=14, sticky=tkinter.W)
    tkinter.Entry(master, textvariable=fluxes_until).grid(column=2, row=14, sticky=tkinter.W)

    # tkinter.Label(master, text="SOURCE: ").grid(column=0, row=13, sticky=tkinter.W)
    # tkinter.Entry(master, textvariable=server_raw_binary_source_dir).grid(column=1, row=13, columnspan=3, sticky=tkinter.W)

    tkinter.Checkbutton(master, text="use RDS", variable=use_rds).grid(column=1, row=15, sticky=tkinter.W)

    tkinter.Button(master, text='Run', padx=40, pady=40, command=master.quit).grid(column=2, row=22, sticky=tkinter.W, pady=4)

    if skip_gui == False:
        master.mainloop()

    # get final settings from GUI
    download_raw_binary_data_from_server = download_raw_binary_data_from_server.get()
    download_how_many_files_max = download_how_many_files_max.get()
    convert_raw = convert_raw.get()
    download_eddypro_settings_from_server = download_eddypro_settings_from_server.get()
    plot_availability = plot_availability.get()
    plot_raw = plot_raw.get()
    calc_fluxes = calc_fluxes.get()
    plot_full_output = plot_full_output.get()
    plot_diurnal_cycles = plot_diurnal_cycles.get()
    use_rds = use_rds.get()
    fluxes_from = fluxes_from.get()
    fluxes_until = fluxes_until.get()
    raw_binary_file_extension = raw_binary_file_extension.get()
    raw_binary_min_filesize_bytes = raw_binary_min_filesize_bytes.get()
    site_to_process = site_to_process.get()
    server_raw_binary_source_dir = server_raw_binary_source_dir.get()

    if download_raw_binary_data_from_server == 1:
        if site_to_process == 'CH-AWS':
            server_raw_binary_source_dir = AWS_raw_binary_source_dir
        elif site_to_process == 'CH-CHA':
            server_raw_binary_source_dir = CHA_raw_binary_source_dir
        elif site_to_process == 'CH-DAV':
            server_raw_binary_source_dir = DAV_raw_binary_source_dir
        elif site_to_process == 'CH-FRU':
            server_raw_binary_source_dir = FRU_raw_binary_source_dir
        elif site_to_process == 'CH-INO':
            server_raw_binary_source_dir = INO_raw_binary_source_dir
        elif site_to_process == 'CH-LAE':
            server_raw_binary_source_dir = LAE_raw_binary_source_dir
        elif site_to_process == 'CH-LAS':
            server_raw_binary_source_dir = LAS_raw_binary_source_dir
        elif site_to_process == 'CH-OE2':
            server_raw_binary_source_dir = OE2_raw_binary_source_dir
        elif site_to_process == 'US-TWE':
            server_raw_binary_source_dir = TWE_raw_binary_source_dir

    # write all settings from GUI to txt file
    settings_out = "settings.txt"
    settings_out_full_path = os.path.join(source_dir, settings_out)
    settings_out_full_path = open(settings_out_full_path, 'w')
    settings_out_full_path.write("download_raw_binary_data_from_server=" + str(download_raw_binary_data_from_server))
    settings_out_full_path.write("\ndownload_how_many_files_max=" + str(download_how_many_files_max))
    settings_out_full_path.write("\nconvert_raw=" + str(convert_raw))
    settings_out_full_path.write("\ndownload_eddypro_settings_from_server=" + str(download_eddypro_settings_from_server))
    settings_out_full_path.write("\nplot_availability=" + str(plot_availability))
    settings_out_full_path.write("\nplot_raw=" + str(plot_raw))
    settings_out_full_path.write("\ncalc_fluxes=" + str(calc_fluxes))
    settings_out_full_path.write("\nplot_full_output=" + str(plot_full_output))
    settings_out_full_path.write("\nplot_diurnal_cycles_output=" + str(plot_diurnal_cycles))
    settings_out_full_path.write("\nuse_rds=" + str(use_rds))
    settings_out_full_path.write("\nfluxes_from=" + str(fluxes_from))
    settings_out_full_path.write("\nfluxes_until=" + str(fluxes_until))
    settings_out_full_path.write("\nraw_binary_file_extension=" + str(raw_binary_file_extension))
    settings_out_full_path.write("\nraw_binary_min_filesize_bytes=" + str(raw_binary_min_filesize_bytes))
    settings_out_full_path.write("\nsite_to_process=" + str(site_to_process))
    settings_out_full_path.write("\nserver_raw_binary_source_dir=" + str(server_raw_binary_source_dir))

    settings_out_full_path.write("\n\nAWS_raw_binary_source_dir=" + str(AWS_raw_binary_source_dir))
    settings_out_full_path.write("\nCHA_raw_binary_source_dir=" + str(CHA_raw_binary_source_dir))
    settings_out_full_path.write("\nDAV_raw_binary_source_dir=" + str(DAV_raw_binary_source_dir))
    settings_out_full_path.write("\nFRU_raw_binary_source_dir=" + str(FRU_raw_binary_source_dir))
    settings_out_full_path.write("\nINO_raw_binary_source_dir=" + str(INO_raw_binary_source_dir))
    settings_out_full_path.write("\nLAE_raw_binary_source_dir=" + str(LAE_raw_binary_source_dir))
    settings_out_full_path.write("\nLAS_raw_binary_source_dir=" + str(LAS_raw_binary_source_dir))
    settings_out_full_path.write("\nOE2_raw_binary_source_dir=" + str(OE2_raw_binary_source_dir))
    settings_out_full_path.write("\nTWE_raw_binary_source_dir=" + str(TWE_raw_binary_source_dir))
    settings_out_full_path.close()

    master.destroy()

    return download_raw_binary_data_from_server, download_how_many_files_max, convert_raw,\
           download_eddypro_settings_from_server, plot_availability, plot_raw, calc_fluxes,\
           plot_full_output, plot_diurnal_cycles, use_rds, fluxes_from, fluxes_until,\
           raw_binary_file_extension, raw_binary_min_filesize_bytes, site_to_process, server_raw_binary_source_dir
