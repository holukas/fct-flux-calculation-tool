import csv
import ftplib

__author__ = 'holukas'

output_date = ()
output_time = ()
output_data = ()

# -----------------| CREATE OUTPUT FILE
spamWriter = open('data.tsv', 'w', newline='')
# writer = csv.writer(spamWriter, delimiter='\t')  # use this for tab delimited output
writer = csv.writer(spamWriter, delimiter=',')  # use this for comma delimited output
writer.writerow(['date', 'close'])

# -----------------| OPEN DATA
with open('E:\\Dropbox\\Dropbox\\luhk_work\\PR_python_projects\\convert_eth_binary_to_ascii\\output_chamau_test1\\eddypro\\results\\eddypro_chamau1_full_output_2014-05-12T205147.csv', newline='') as csvfile:
    rows = csv.reader(csvfile, delimiter=',')
    row_counter = 0
    for row in rows:
        row_counter += 1

        if row_counter == 2:  # main header are in row 2
            date_column = row.index('date')
            time_column = row.index('time')
            output_column = row.index('co2_flux')

        if row_counter > 3:  # data start in row 4
            date = row[date_column]
            time = row[time_column]
            # output_timestamp = date + "_" + time
            # output_timestamp = output_timestamp.replace('-', '').replace(':', '')
            output_timestamp = date
            output_timestamp = output_timestamp.replace('-', '').replace(':', '')
            output_data = row[output_column]
            out = [output_timestamp] + [output_data]
            writer.writerow(out)

csvfile.close()
spamWriter.close()

# -----------------| UPLOAD
ftp = ftplib.FTP('XXX', 'XXX', 'XXX')
ftp.cwd("/chart_test2")
print("File List: ")
files = ftp.dir()  # show directory file list
print(files)
ftp.storbinary('STOR data.tsv', open('data.tsv', 'rb'))

print("done.")