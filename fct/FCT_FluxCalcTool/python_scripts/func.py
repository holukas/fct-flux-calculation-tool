import datetime
import fnmatch
import os
import FCT_FluxCalcTool.info.version_info as version_info


def intro_screen():

    version_info_dict = version_info.version_info()

    print("==============================")
    print("ETH FluxCalcTool using EddyPro")
    print("==============================")
    print("\n")
    print(f"FluxCalcTool: {version_info_dict['FluxCalcTool']}")
    print(f"EddyPro: {version_info_dict['EddyPro']}")
    print(f"last edit: {version_info_dict['last edit']}")
    print(f"source code: {version_info_dict['source code']}")
    print("--------------------------")
    print("\n")
    print("                                      ,,,,,,")
    print("                                 o#'9MMHb':'-,o,")
    print("                              .oH\":HH$' \"' ' -*R&o,")
    print("                            dMMM*\"\"'`'      .oM\"HM?.")
    print("                           ,MMM'          \"HLbd< ?&H\"")
    print("                          .:MH .\"\          ` MM  MM&b\"")
    print("                         . \"*H    -        &MMMMMMMMMH:\"")
    print("                         .    dboo        MMMMMMMMMMMM.")
    print("                         .   dMMMMMMb      *MMMMMMMMMP.")
    print("                         .    MMMMMMMP        *MMMMMP .")
    print("                              `#MMMMM           MM6P ,")
    print("                          '    `MMMP\"           HM*`,\"")
    print("                           '    :MM             .- ,")
    print("                            '.   `#?..  .       ..'")
    print("                               -.   .         .-")
    print("                                 ''-.oo,oo.-''")
    print("\n")
    print("Starting conversions and calculation...")
    # os.chdir('python_scripts/')
    # os.system('main.py')
    # input("\nI finished your calculations.")



def range_gain_finder(wind_range):
    if wind_range == '11':
        range_gain = 0.25
    elif wind_range == '10':
        range_gain = 0.50
    elif wind_range == '01':
        range_gain = 1
    elif wind_range == '00':
        range_gain = 2
    return range_gain


# def send_email(to):
#     # example from: https://docs.python.org/3/library/email-examples.html
#     #!/usr/bin/env python3
#
#     import smtplib
#
#     from email.mime.multipart import MIMEMultipart
#     from email.mime.text import MIMEText
#
#     # me == my email address
#     # you == recipient's email address
#     me = "no-reply@email.com"
#     you = "lukas.hoertnagl@usys.ethz.ch"
#
#     # Create message container - the correct MIME type is multipart/alternative.
#     msg = MIMEMultipart('alternative')
#     msg['Subject'] = "Link"
#     msg['From'] = me
#     msg['To'] = you
#
#     # Create the body of the message (a plain-text and an HTML version).
#     text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttps://www.python.org"
#     html = """\
#     <html>
#       <head></head>
#       <body>
#         <p>Hi!<br>
#            How are you?<br>
#            Here is the <a href="https://www.python.org">link</a> you wanted.
#         </p>
#       </body>
#     </html>
#     """
#
#     # Record the MIME types of both parts - text/plain and text/html.
#     part1 = MIMEText(text, 'plain')
#     part2 = MIMEText(html, 'html')
#
#     # Attach parts into message container.
#     # According to RFC 2046, the last part of a multipart message, in this case
#     # the HTML message, is best and preferred.
#     msg.attach(part1)
#     msg.attach(part2)
#
#     # Send the message via local SMTP server.
#     s = smtplib.SMTP('127.0.0.1')
#     # sendmail function takes 3 arguments: sender's address, recipient's address
#     # and message to send - here it is sent as one string.
#     s.sendmail(me, you, msg.as_string())
#     s.quit()

def generate_id_string():
    # current time
    now = datetime.datetime.now()
    now_year = now.year
    now_month = now.month
    now_day = now.day
    now_hour = now.hour
    now_minute = now.minute
    now_second = now.second
    now_string = str(now_year) + "-" + str(now_month).zfill(2) + "-" + str(now_day).zfill(2) + "T" + str(
        now_hour).zfill(2) + str(now_minute).zfill(2) + str(now_second).zfill(2)
    # now_string = str(now_year) + "-" + str(now_month).zfill(2) + "-" + str(now_day).zfill(2) + "T" + str(
    #     now_hour).zfill(2) + str(now_minute).zfill(2) + str(now_second).zfill(2)

    return now_string


def check_if_file_in_folder(search_file, folder):

    # check if filename exists in a folder

    found = False
    check_output_files = os.listdir(folder)
    for output_file in check_output_files:
        if fnmatch.fnmatch(output_file, search_file):
            found = True
            break

    return found