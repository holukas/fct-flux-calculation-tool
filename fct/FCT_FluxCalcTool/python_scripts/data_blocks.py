import struct

import FCT_FluxCalcTool.python_scripts.func as func


def data_block_header(open_file_object, size_header):  # size: n bytes
    if size_header == 29:
        # header is 29 bytes
        byte = open_file_object.read(29)
        print(str(len(byte)))
        s = struct.Struct('<c B c L B B B B B B B B B B B B B B B B B B L')
        unpacked_data = s.unpack(byte)

        # contents of header according to sonic manual and Werner Eugster's sonicread.pdf
        header_file_type = unpacked_data[0]
        header_file_version = unpacked_data[1]
        header_anemometer_type_string = unpacked_data[2]

        header_serial_number = unpacked_data[3]
        header_average = unpacked_data[4]
        header_wind_report_mode = unpacked_data[5]

        header_string_format = unpacked_data[6]
        header_ascii_terminator = unpacked_data[7]
        header_echo = unpacked_data[8]

        header_message_mode = unpacked_data[9]
        header_confidence_tone = unpacked_data[10]
        header_axis_alignment = unpacked_data[11]

        header_speed_of_sound_mode = unpacked_data[12]
        header_absolute_temperature_mode = unpacked_data[13]
        header_analogue_inputs_enabled = unpacked_data[14:20]

        header_analogue_output_fsd = unpacked_data[20]
        header_direction_wrap_mode = unpacked_data[21]
        header_beginning = unpacked_data[22]

        print("===================")
        print("HEADER INFORMATION:")
        print("===================")
        print("HEADER SIZE: " + str(size_header))
        print("---")
        print("header_file_type: " + str(header_file_type))
        print("header_file_version: " + str(header_file_version))
        print("header_anemometer_type_string: " + str(header_anemometer_type_string))

        print("header_serial_number: " + str(header_serial_number))
        print("header_average: " + str(header_average))
        print("header_wind_report_mode: " + str(header_wind_report_mode))

        print("header_string_format: " + str(header_string_format))
        print("header_ascii_terminator: " + str(header_ascii_terminator))
        print("header_echo: " + str(header_echo))

        print("header_message_mode: " + str(header_message_mode))
        print("header_confidence_tone: " + str(header_confidence_tone))
        print("header_axis_alignment: " + str(header_axis_alignment))

        print("header_speed_of_sound_mode: " + str(header_speed_of_sound_mode))
        print("header_absolute_temperature_mode: " + str(header_absolute_temperature_mode))
        print("header_analogue_inputs_enabled: " + str(header_analogue_inputs_enabled))

        print("header_analogue_output_fsd: " + str(header_analogue_output_fsd))
        print("header_direction_wrap_mode: " + str(header_direction_wrap_mode))
        print("header_beginning: " + str(header_beginning))

    elif size_header == 38:
        # header is 38 bytes
        byte = open_file_object.read(38)
        print(str(len(byte)))
        s = struct.Struct('<c c c c c c B B B B B B B B B B B B B B B B B B c c c c c c c c c c B B B B')
        unpacked_data = s.unpack(byte)

        # contents of header according to sonic manual and Werner Eugster's sonicread.pdf
        header_format = unpacked_data[0:6]  # will hold the characters WESAT3
        header_file_version = unpacked_data[6]
        header_file_type = unpacked_data[7]

        header_firmware_version = unpacked_data[8:14]
        header_serial_number = unpacked_data[14:18]
        header_execution_para = unpacked_data[18]

        header_trigger_src = unpacked_data[19]
        header_analog_range = unpacked_data[20]
        header_data_status = unpacked_data[21]

        header_terminal_mode = unpacked_data[22]
        header_prompt = unpacked_data[23]
        header_hostname = unpacked_data[23:33]

        header_file_create_time = unpacked_data[33:]

        print("===================")
        print("HEADER INFORMATION:")
        print("===================")
        print("HEADER SIZE: " + str(size_header))
        print("---")
        print("header_format: " + str(header_format))
        print("header_file_version: " + str(header_file_version))
        print("header_file_type: " + str(header_file_type))

        print("header_firmware_version: " + str(header_firmware_version))
        print("header_serial_number: " + str(header_serial_number))
        print("header_execution_para: " + str(header_execution_para))

        print("header_trigger_src: " + str(header_trigger_src))
        print("header_analog_range: " + str(header_analog_range))
        print("header_data_status: " + str(header_data_status))

        print("header_terminal_mode: " + str(header_terminal_mode))
        print("header_prompt: " + str(header_prompt))
        print("header_hostname: " + str(header_hostname))

        print("header_file_create_time: " + str(header_file_create_time))

        # typedef struct			/* the new WESAT3 header size is 38 bytes		*/
        # 	{
        # 	char		format[6];	/* will hold the characters WESAT3		*/
        # 	unsigned char	file_version;	/* in case the WESAT3 header needs changes	*/
        # 	unsigned char	file_type;
        # 	unsigned char	firmware_version[6];
        # 	unsigned char	serial_number[4];
        # 	unsigned char	execution_para;
        # 	unsigned char	trigger_src;
        # 	unsigned char	analog_range;
        # 	unsigned char	data_status;
        # 	unsigned char	terminal_mode;
        # 	unsigned char	prompt;
        # 	char		hostname[10];
        # 	unsigned char	file_create_time[4];
        # 	} HEADERTYPECSAT3;


def data_block_sonic_r3_50(open_file_object):  # size: 12 bytes
    # print('------- SONIC ANEMOMETER information')
    # byte = open_file_object.read(8)  # for checking only
    # s = struct.Struct('B B B B B B B B')  # for checking only
    # unpacked_data = s.unpack(byte)  # for checking only
    # print(unpacked_data)  # for checking only
    byte = open_file_object.read(12)
    if len(byte) == 12:
        s = struct.Struct('>h h h h h h')  # big endian short integer
        # Big-endian systems store the most significant byte of a word in the smallest address
        unpacked_data = s.unpack(byte)
        u = unpacked_data[0] / 100
        v = unpacked_data[1] / 100
        w = unpacked_data[2] / 100
        ts = unpacked_data[3] / 100
        inc_x = unpacked_data[4]
        inc_y = unpacked_data[5]

        return u, v, w, ts, inc_x, inc_y


def data_block_sonic_r2_1_with_extrabyte(open_file_object):  # size: 13 bytes
    # "r2_1" is the internal name for EddyPro if 1 sonic r2 is used
    # print('------- SONIC ANEMOMETER information')
    byte = open_file_object.read(13)  # contains extra byte!

    # # test:
    # byte = open_file_object.read(29)
    # s = struct.Struct('>h h h h h h B B B B B B B B B B B B B B B B B')
    # unpacked_data = s.unpack(byte)
    # print(unpacked_data)
    # # test shows that the extra byte is right after speed of sound and before inclinometers

    if len(byte) == 13:
        s = struct.Struct('>h h h h B h h')  # big endian short integer
        # Big-endian systems store the most significant byte of a word in the smallest address
        unpacked_data = s.unpack(byte)
        u = unpacked_data[0] / 100
        v = unpacked_data[1] / 100
        w = unpacked_data[2] / 100
        ts = unpacked_data[3] * 0.02  # see explanation below
        ts *= ts
        ts /= 403
        ts -= 273.15  # in C°
        # extra_byte = unpacked_data[4]  # not needed for output
        inc_x = unpacked_data[5]
        inc_y = unpacked_data[6]

        return u, v, w, ts, inc_x, inc_y

    # regarding ts --> C code from ethconvert:
    # if(is_R2_sonic){
    #   c=0.02*dataWECOM3.Tv;
    #   Tv=c*c/403.0-273.15;	/* Tv in degrees C */

    #  When we record data from an old Solent R2 or R2A sonic (-R2 command line
    # option of sonicreadHS) then we ﬁnd the values 99 or 98 in the file version ﬁeld, and
    # serial number is set to the value 0x09090909. The ﬁle version 99 has two issues that need
    # to be known: (a) after the base data (wind, speed of sound, analog channels) there might be
    # an extra byte that should not be there, followed by 4 bytes with 0x00 that correspond to the
    # inclinometer variable that is however not available from R2/R2A sonics. In sonicreadHS
    # version 5.07 we modiﬁed this and eliminated the erroneous extra byte from R2 sonics, and
    # omitted the 4 empty bytes of the inclinometer variables.


def data_block_sonic_r2_1_without_extrabyte(open_file_object):  # size: 13 bytes
    # "r2_1" is the internal name for EddyPro if 1 sonic r2 is used
    byte = open_file_object.read(8)  # contains extra byte!

    if len(byte) == 8:
        s = struct.Struct('>h h h h')  # big endian short integer
        # Big-endian systems store the most significant byte of a word in the smallest address
        unpacked_data = s.unpack(byte)
        u = unpacked_data[0] / 100
        v = unpacked_data[1] / 100
        w = unpacked_data[2] / 100
        ts = unpacked_data[3] * 0.02  # see explanation below
        ts *= ts
        ts /= 403
        ts -= 273.15  # in C°
        # extra_byte = unpacked_data[4]  # not needed for output
        # inc_x = unpacked_data[4]
        # inc_y = unpacked_data[5]

        return u, v, w, ts


def data_block_sonic_hs_50(open_file_object):  # size: 12 bytes
    # print('------- SONIC ANEMOMETER information')
    byte = open_file_object.read(12)
    # print(len(byte))
    if len(byte) == 12:  # checks if we still have data
        s = struct.Struct('>h h h h h h')  # big endian short integer
        # Big-endian systems store the most significant byte of a word in the smallest address
        unpacked_data = s.unpack(byte)
        u = unpacked_data[0] / 100
        v = unpacked_data[1] / 100
        w = unpacked_data[2] / 100
        ts = unpacked_data[3] / 100
        inc_x = unpacked_data[4]
        inc_y = unpacked_data[5]

        return u, v, w, ts, inc_x, inc_y


def data_block_sonic_hs_100(open_file_object):  # size: 12 bytes, same as HS-50
    # print('------- SONIC ANEMOMETER information')
    byte = open_file_object.read(12)
    # print(len(byte))
    if len(byte) == 12:  # checks if we still have data
        s = struct.Struct('>h h h h h h')  # big endian short integer
        # Big-endian systems store the most significant byte of a word in the smallest address
        unpacked_data = s.unpack(byte)
        u = unpacked_data[0] / 100
        v = unpacked_data[1] / 100
        w = unpacked_data[2] / 100
        ts = unpacked_data[3] / 100
        inc_x = unpacked_data[4]
        inc_y = unpacked_data[5]

        return u, v, w, ts, inc_x, inc_y


def data_block_sonic_csat3(open_file_object):  # size: 5 columns / 10 bytes
    # see Table 12 in sonicread.pdf

    # print('------- SONIC ANEMOMETER information')
    byte = open_file_object.read(10)
    # print(len(byte))
    if len(byte) == 10:  # checks if we still have data
        s = struct.Struct('<h h h h H')  # here: little endian order
        # Big-endian systems store the most significant byte of a word in the smallest address
        # for more information please see the CSAT3 manual, Appendix B
        unpacked_data = s.unpack(byte)
        u = unpacked_data[0] * 0.001  # word 0
        v = unpacked_data[1] * 0.001  # word 1
        w = unpacked_data[2] * 0.001  # word 2
        ts = unpacked_data[3] * 0.001 + 340  # word 3; conversion gives Kelvin
        ts = ts * ts / 1.402 / 287.04 - 273.15  # gives degree Celsius; from ethconvert.h: 1.402 = cp/cv, adiabatic exponent; 287.04 = specific gas constant, J kg-1 K-1

        # Decoding word 4:
        diagnostic_word = unpacked_data[4]  # word 4; contains diagnostic information and range of u, v and w
        diagnostic_word = str(
            bin(diagnostic_word)[2:].zfill(16))  # fill with zeros so binary code has 16 digits = 2 Bytes
        # print("diagnostic_word (binary): " + diagnostic_word)
        # diagnostic_flags = diagnostic_word[0:4]
        u_range = diagnostic_word[4:6]
        v_range = diagnostic_word[6:8]
        w_range = diagnostic_word[8:10]
        counter = diagnostic_word[10:]
        # print("diagnostic_flags (binary): " + diagnostic_flags)  # these will not be output to the converted file
        # print("u_range (binary): " + u_range)
        # print("v_range (binary): " + v_range)
        # print("w_range (binary): " + w_range)
        # print("counter (binary): " + counter)

        # Conversions:
        gain = func.range_gain_finder(u_range)
        u = u * gain
        gain = func.range_gain_finder(v_range)
        v = v * gain
        gain = func.range_gain_finder(w_range)
        w = w * gain
        counter = int(str(counter), 2)

        # # check output
        # print("u: " + str(u))
        # print("v: " + str(v))
        # print("w: " + str(w))
        # print("Ts: " + str(ts))
        # print("counter: " + str(counter))

        return u, v, w, ts, counter  # diagnostic_word  #, delimiters


def data_block_irga_li6262(open_file_object):  # size: 16 bytes
    # print('------- Li6262 IRGA information')
    # byte 1: number of bytes in Licor 7500 record (2 = missing, 16 = available)
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]
            status_code = unpacked_data[1]
            if data_size == 16:  # if no missing data
                byte = open_file_object.read(14)
                if len(byte) == 14:  # check if we really have 14 more bytes for li75
                    # byte = open_file_object.read(14)  # for checking only
                    s = struct.Struct('B B B B B B B B B B B B B B')
                    unpacked_data = s.unpack(byte)
                    # print(unpacked_data)  # for checking only
                    status_byte = unpacked_data[0]  # this is the AGC (window dirtiness)
                    binary = bin(status_byte)[2:].zfill(8)
                    lower_nibble = binary[4:]
                    decimal = int(str(lower_nibble), 2)
                    status_byte = (decimal * 6.25)  # + 6.25  # AGC, window dirtiness, offset not used in ETH Flux
                    h2o_mmol_m3 = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[3])
                    co2_mmol_m3 = 0.0001 * (
                            (unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[6])
                    temp_deg_c = 0.01 * ((unpacked_data[7] * 256) + unpacked_data[8])  # K
                    temp_deg_c -= 100  # K, offset
                    press_hpa = 10 * ((unpacked_data[9] * 256) + unpacked_data[10])  # Pa
                    press_hpa *= 0.01  # to hPa
                    cooler_v = 0.0001 * (
                            (unpacked_data[11] * 256 * 256) + (unpacked_data[12] * 256) + unpacked_data[13])

                    return data_size, status_code, status_byte, h2o_mmol_m3, co2_mmol_m3, temp_deg_c, press_hpa, cooler_v
                else:  # this can be the case if data storage stopped abruptly
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

            else:
                if data_size == 2:  # 2015-04-22  # this is the normal case when rest of records are missing
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size > 0:  # 2015-04-22  # 2015-04-24
                    print("!ATTENTION! IRGA LI-7500 data size is not 2 or 16 bytes. Found data_size = " + str(
                        data_size) + ".")
                    # this is implemented due to a problem in data logging at the field site CH-DAV
                    # during the time periods 2012b and 2013a
                    # where it was / is possible that two Li-7200 data blocks arrive in succession
                    # in the binary file, i.e. the sequence for one line was:
                    # r3-li72-li72 (the li75 was missing at the time)
                    # to account for this problem we just read the data size of the li72 (25 Bytes total)
                    # but output the missing values block for the li75
                    # this means, our "reading window" does not shift and is in the right order;
                    # note that this generates data_size = 25 in the output file
                    # and not 16 OR 2 like usual for the li75
                    for xx in range(0, data_size - 2):
                        # print(xx)
                        # construct binary reader for data_size number of bytes
                        byte = open_file_object.read(1)
                        if len(byte) == 1:  # 2015-04-24
                            s = struct.Struct('B')
                            unpacked_data = s.unpack(byte)
                            # print(unpacked_data)
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size == 0:  # 2015-04-24
                    print("!ATTENTION! IRGA LI-7500 data size is not 2 or 16 bytes. Found data_size = 0.")
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_irga_li7500(open_file_object):  # size: 16 bytes
    # print('------- Li7500 IRGA information')
    # byte 1: number of bytes in Licor 7500 record (2 = missing, 16 = available)
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]
            status_code = unpacked_data[1]
            if data_size == 16:  # if no missing data
                byte = open_file_object.read(14)
                if len(byte) == 14:  # check if we really have 14 more bytes for li75
                    # byte = open_file_object.read(14)  # for checking only
                    s = struct.Struct('B B B B B B B B B B B B B B')
                    unpacked_data = s.unpack(byte)
                    # print(unpacked_data)  # for checking only
                    status_byte = unpacked_data[0]  # this is the AGC (window dirtiness)
                    binary = bin(status_byte)[2:].zfill(8)
                    lower_nibble = binary[4:]
                    decimal = int(str(lower_nibble), 2)
                    status_byte = (decimal * 6.25)  # + 6.25  # AGC, window dirtiness, offset not used in ETH Flux
                    h2o_mmol_m3 = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[3])
                    co2_mmol_m3 = 0.0001 * (
                            (unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[6])
                    temp_deg_c = 0.01 * ((unpacked_data[7] * 256) + unpacked_data[8])  # K
                    temp_deg_c -= 100  # K, offset
                    press_hpa = 10 * ((unpacked_data[9] * 256) + unpacked_data[10])  # Pa
                    press_hpa *= 0.01  # to hPa
                    cooler_v = 0.0001 * (
                            (unpacked_data[11] * 256 * 256) + (unpacked_data[12] * 256) + unpacked_data[13])

                    return data_size, status_code, status_byte, h2o_mmol_m3, co2_mmol_m3, temp_deg_c, press_hpa, cooler_v
                else:  # this can be the case if data storage stopped abruptly
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

            else:
                if data_size == 2:  # 2015-04-22  # this is the normal case when rest of records are missing
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size > 0:  # 2015-04-22  # 2015-04-24
                    # print("!ATTENTION! IRGA LI-7500 data size is not 2 or 16 bytes. Found data_size = " + str(data_size) + ".") todo

                    # this is implemented due to a problem in data logging at the field site CH-DAV
                    # during the time periods 2012b and 2013a
                    # where it was / is possible that two Li-7200 data blocks arrive in succession
                    # in the binary file, i.e. the sequence for one line was:
                    # r3-li72-li72 (the li75 was missing at the time)
                    # to account for this problem we just read the data size of the li72 (25 Bytes total)
                    # but output the missing values block for the li75
                    # this means, our "reading window" does not shift and is in the right order;
                    # note that this generates data_size = 25 in the output file
                    # and not 16 OR 2 like usual for the li75
                    for xx in range(0, data_size - 2):
                        # print(xx)
                        # construct binary reader for data_size number of bytes
                        byte = open_file_object.read(1)
                        if len(byte) == 1:  # 2015-04-24
                            s = struct.Struct('B')
                            unpacked_data = s.unpack(byte)
                            # print(unpacked_data)
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size == 0:  # 2015-04-24
                    print("!ATTENTION! IRGA LI-7500 data size is not 2 or 16 bytes. Found data_size = 0.")
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_irga_li7500_co2_gain0974(open_file_object):  # size: 16 bytes
    """Apply gain 0.974 to CO2 concentration measurements to correct for the
    usage of a wrong calibration gas.

    Data block added in v0.9.5

    For more info see here:
    https://www.swissfluxnet.ethz.ch/index.php/knowledge-base/wrong-calibration-gas-2017/
    """
    # byte 1: number of bytes in Licor 7500 record (2 = missing, 16 = available)
    calib_gas_corr_gain = 0.974

    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]
            status_code = unpacked_data[1]
            if data_size == 16:  # if no missing data
                byte = open_file_object.read(14)
                if len(byte) == 14:  # check if we really have 14 more bytes for li75
                    # byte = open_file_object.read(14)  # for checking only
                    s = struct.Struct('B B B B B B B B B B B B B B')
                    unpacked_data = s.unpack(byte)
                    # print(unpacked_data)  # for checking only
                    status_byte = unpacked_data[0]  # this is the AGC (window dirtiness)
                    binary = bin(status_byte)[2:].zfill(8)
                    lower_nibble = binary[4:]
                    decimal = int(str(lower_nibble), 2)
                    status_byte = (decimal * 6.25)  # + 6.25  # AGC, window dirtiness, offset not used in ETH Flux
                    h2o_mmol_m3 = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[3])
                    co2_mmol_m3 = 0.0001 * (
                            (unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[6])
                    co2_mmol_m3 = co2_mmol_m3 * calib_gas_corr_gain
                    temp_deg_c = 0.01 * ((unpacked_data[7] * 256) + unpacked_data[8])  # K
                    temp_deg_c -= 100  # K, offset
                    press_hpa = 10 * ((unpacked_data[9] * 256) + unpacked_data[10])  # Pa
                    press_hpa *= 0.01  # to hPa
                    cooler_v = 0.0001 * (
                            (unpacked_data[11] * 256 * 256) + (unpacked_data[12] * 256) + unpacked_data[13])

                    return data_size, status_code, status_byte, h2o_mmol_m3, co2_mmol_m3, temp_deg_c, press_hpa, cooler_v
                else:  # this can be the case if data storage stopped abruptly
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

            else:
                if data_size == 2:  # 2015-04-22  # this is the normal case when rest of records are missing
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size > 0:  # 2015-04-22  # 2015-04-24
                    # print("!ATTENTION! IRGA LI-7500 data size is not 2 or 16 bytes. Found data_size = " + str(data_size) + ".") todo

                    # this is implemented due to a problem in data logging at the field site CH-DAV
                    # during the time periods 2012b and 2013a
                    # where it was / is possible that two Li-7200 data blocks arrive in succession
                    # in the binary file, i.e. the sequence for one line was:
                    # r3-li72-li72 (the li75 was missing at the time)
                    # to account for this problem we just read the data size of the li72 (25 Bytes total)
                    # but output the missing values block for the li75
                    # this means, our "reading window" does not shift and is in the right order;
                    # note that this generates data_size = 25 in the output file
                    # and not 16 OR 2 like usual for the li75
                    for xx in range(0, data_size - 2):
                        # print(xx)
                        # construct binary reader for data_size number of bytes
                        byte = open_file_object.read(1)
                        if len(byte) == 1:  # 2015-04-24
                            s = struct.Struct('B')
                            unpacked_data = s.unpack(byte)
                            # print(unpacked_data)
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size == 0:  # 2015-04-24
                    print("!ATTENTION! IRGA LI-7500 data size is not 2 or 16 bytes. Found data_size = 0.")
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_sonic_hs_50_extended(open_file_object):  # size: 12 bytes
    """
        Extended data logging to comply w/ ICOS requirements.
        Installed in CH-DAV in 2017-07.

        from Werner Eugster email 2017-07-10:
        the former 4 data bytes containing the two inclinometer angles are now used differently
        * first byte is the StaA byte from the sonic
        * second byte is the StaD byte from the sonic
        * third and fourth byte are still a short integer as before, but now
            it contains the inclinometer X angle if the record number is an odd number,
            or the inclinometer Y angle if the record number is an even number
    """
    byte = open_file_object.read(12)
    # print(len(byte))
    if len(byte) == 12:  # checks if we still have data

        # https://docs.python.org/3.1/library/struct.html#byte-order-size-and-alignment
        # B...unsigned char, integer, 1 Byte
        # h...short integer, 2 Bytes
        # >...big-endian, MSB at lowest address

        # 2017-07-25: EddyPro can currently not handle StaA and StaD, but it will be implemented soon for ICOS requirements
        s = struct.Struct('>h h h h B B h')
        # Big-endian systems store the most significant byte of a word in the smallest address
        unpacked_data = s.unpack(byte)
        u = unpacked_data[0] / 100
        v = unpacked_data[1] / 100
        w = unpacked_data[2] / 100
        ts = unpacked_data[3] / 100

        StaA = unpacked_data[4]
        StaD = unpacked_data[5]
        # StaA_bin = bin(StaA)[2:].zfill(8)
        # StaD_bin = bin(StaD)[2:].zfill(8)
        # dummy = int(str(dummy), 2)  # convert bin to int

        inc = unpacked_data[6]  # inclinometer, x or y (depending on record)

        return u, v, w, ts, StaA, StaD, inc


# def data_block_sonic_hs_50_extended_synctest(open_file_object):  # size: 12 bytes
#     """
#         During ICOS sync test Oct 2018
#     """
#     byte = open_file_object.read(16)
#     # print(len(byte))
#     if len(byte) == 16:  # checks if we still have data
#
#         # https://docs.python.org/3.1/library/struct.html#byte-order-size-and-alignment
#         # B...unsigned char, integer, 1 Byte
#         # h...short integer, 2 Bytes
#         # >...big-endian, MSB at lowest address
#
#         # 2017-07-25: EddyPro can currently not handle StaA and StaD, but it will be implemented soon for ICOS requirements
#         s = struct.Struct('>h h h h h h B B h')
#         # Big-endian systems store the most significant byte of a word in the smallest address
#         unpacked_data = s.unpack(byte)
#         u = unpacked_data[0] / 100
#         v = unpacked_data[1] / 100
#         w = unpacked_data[2] / 100
#         ts = unpacked_data[3] / 100
#
#         sync1 = unpacked_data[4]
#         sync2 = unpacked_data[5]
#
#         StaA = unpacked_data[6]
#         StaD = unpacked_data[7]
#         # StaA_bin = bin(StaA)[2:].zfill(8)
#         # StaD_bin = bin(StaD)[2:].zfill(8)
#         # dummy = int(str(dummy), 2)  # convert bin to int
#
#         inc = unpacked_data[8]  # inclinometer, x or y (depending on record)
#
#         print(u, v, w)
#
#         return u, v, w, ts, sync1, sync2, StaA, StaD, inc


# def data_block_irga_li7200_extended_synctest(open_file_object):  # size: 12 columns / 26 bytes
#     """
#         For CH-DAV on Oct 2018
#         During the sync tests required by ICOS, the data block consisted of
#         two different columns with analogue signals.
#         Different columns: AUX3, AUX1
#         Missing columns: CO2MFD, VolFlowRate ...
#         So it still has the same number of output columns.
#     """
#     # byte 1: number of bytes in Licor 7200 record (2 = missing, 26 = available)
#     # B...unsigned char, integer, 1 Byte
#     # h...short integer, 2 Bytes
#     # >...big-endian, MSB at lowest address
#
#     byte = open_file_object.read(2)
#     if byte:  # if there are data to read
#         s = struct.Struct('B B')
#         if len(byte) == 2:
#             unpacked_data = s.unpack(byte)
#
#             # col 1/13
#             data_size = unpacked_data[0]
#
#             # col 2/13
#             status_code = unpacked_data[1]
#
#             if data_size == 26:  # if no missing data
#                 # read next 24 bytes for a total of 26 bytes / byte 3 is OCTAL, todo correct output for octal
#                 byte = open_file_object.read(24)
#                 if len(byte) == 24:  # check if we really have 24 more bytes
#
#                     s = struct.Struct('>h B B B B B B B B B B B B B B B B B B B B B B')
#                     unpacked_data = s.unpack(byte)
#
#                     # DIAGNOSTIC VALUE
#                     # Li72 manual: "The cell diagnostic value is a 2 byte unsigned integer (value between 0 and 8191)"
#
#                     # col 3/13
#                     diag_val = unpacked_data[0]  # diagnostic value, MSB!
#
#                     # extract info from diag_val; for the output, we use only the diag_val integer value
#                     # exception: signal strength is part of the default output
#                     diag_val_bin = bin(diag_val)[2:].zfill(16)
#
#                     # bits 13-15: always read 0
#                     unused = diag_val_bin[0:3]
#
#                     # bit 12:   sensor head attached to LI-7550; 1 = LI-7200
#                     head_detect = diag_val_bin[3:4]
#
#                     # bit 11:   1 = thermocouple OK; 0 = thermocouple open circuit
#                     Toutlet = diag_val_bin[4:5]
#
#                     # bit 10:   1 = thermocouple OK; 0 = thermocouple open circuit
#                     Tintlet = diag_val_bin[5:6]
#
#                     # bit 9:    1 = internal reference voltages OK;
#                     #           0 = internal reference voltages not OK, analyzer interface unit needs service
#                     aux_input = diag_val_bin[6:7]
#
#                     # bit 8:    1 = good, 0.1 to 4.9V; 0 = out of range; d=delta
#                     dpressure = diag_val_bin[7:8]
#
#                     # bit 7:    1 = chopper wheel temp is near setpoint;
#                     #           0 = not near setpoint
#                     chopper = diag_val_bin[8:9]
#
#                     # bit 6:    1 = detector temp is near setpoint
#                     #           0 = not near setpoint
#                     detector = diag_val_bin[9:10]
#
#                     # bit 5:    1 = OK; lock bit, indicates that optical wheel is rotating at the correct rate
#                     PLL = diag_val_bin[10:11]
#
#                     # bit 4:    always set to 1 (OK)
#                     sync = diag_val_bin[11:12]
#
#                     # col 4/13
#                     # bits 0-3: value x 6.67 = signal strength
#                     signal_strength = diag_val_bin[12:16]
#                     signal_strength = int(str(signal_strength), 2)  # convert bin to int
#                     signal_strength = (signal_strength * 6.67)
#
#                     # col 5/13  COOLER V
#                     cooler_v = 0.001 * ((unpacked_data[1] * 256) + (unpacked_data[2]))
#
#                     # col 6/13  AUX3
#                     aux3 = 1 * ((unpacked_data[3] * 256 * 256) + (unpacked_data[4] * 256) + unpacked_data[5])
#
#                     # col 7/13  H2OD
#                     h2o_mmol_m3 = 0.001 * ((unpacked_data[6] * 256 * 256) + (unpacked_data[7] * 256) + unpacked_data[8])
#
#
#                     # dry mole fraction, mmol mol-1; col 5/13
#                     h2o_ppt = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[3])
#
#                     # dry mole fraction, umol mol-1; col 6/13
#                     co2_ppm = 0.0001 * ((unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[6])
#
#                     # # col 7/13
#                     # h2o_mmol_m3 = 0.001 * ((unpacked_data[7] * 256 * 256) + (unpacked_data[8] * 256) + unpacked_data[9])
#
#                     # col 8/13
#                     co2_mmol_m3 =\
#                         0.0001 * ((unpacked_data[10] * 256 * 256) + (unpacked_data[11] * 256) + unpacked_data[12])
#
#                     # offset; col 9/13
#                     cell_temp_in_deg_c = 0.01 * ((unpacked_data[13] * 256) + unpacked_data[14])
#                     cell_temp_in_deg_c -= 100
#
#                     # to hPa; col 10/13
#                     cell_press_hpa = 10 * ((unpacked_data[15] * 256) + unpacked_data[16])  # Pa
#                     cell_press_hpa *= 0.01
#
#                     # to hPa; col 11/13
#                     atm_press_hpa = 10 * ((unpacked_data[17] * 256) + unpacked_data[18])  # Pa
#                     atm_press_hpa *= 0.01
#
#                     # col 12/13
#                     # cooler_v = 0.001 * ((unpacked_data[19] * 256) + (unpacked_data[20]))
#
#                     # col 13/13
#                     flow_l_min = 0.001 * ((unpacked_data[21] * 256) + (unpacked_data[22]))
#
#                     return data_size, status_code, diag_val, signal_strength, h2o_ppt, co2_ppm, h2o_mmol_m3, \
#                            co2_mmol_m3, cell_temp_in_deg_c, cell_press_hpa, atm_press_hpa, cooler_v, flow_l_min
#                 else:
#                     return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
#                            '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
#
#             else:
#                 # -----------------------------------------------
#                 # this part was included in v 0.86
#                 # necessary b/c in CH-DAV starting in Oct 2018 sometimes a 16 Byte data_size is detected
#                 # the exact reason for this is unknown, but it seems like the sonicread script tries
#                 # to store an IRGA75 data block (but IRGA75 is not installed at the site)
#                 # therefore, in this part here, we look at the data size that is given in the file (e.g. 16 Bytes),
#                 # then read that given number of Bytes minus 2 bytes (b/c the first two Bytes were already read)
#                 # from the file, and then output the normal missing data header.
#                 # a similar case was reported above for the IRGA75 (see description in data block).
#                 # -----------------------------------------------
#
#                 if data_size == 2:
#                     return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
#                            '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
#                 elif data_size > 0:
#                     # in case the data size is different than the typical 2 or 26 Bytes, the given number
#                     # of Bytes is read from the file and the regular header is output
#                     for xx in range(0, data_size - 2):
#                         byte = open_file_object.read(1)
#                         if len(byte) == 1:  # 2015-04-24
#                             s = struct.Struct('B')
#                             unpacked_data = s.unpack(byte)
#                     return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
#                            '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
#
#                 elif data_size == 0:
#                     print("!ATTENTION! IRGA LI-7200 data size is not 2 or 26 bytes. Found data_size = 0.")
#                     return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
#                            '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_irga_li7200_extended(open_file_object):  # size: 12 columns / 25 bytes
    """
        Extended data logging to comply w/ ICOS requirements.
        Installed in CH-DAV in 2017-07.
        Returns 13 columns.

        from Werner Eugster email 2017-07-10:
        The length of the LI7200 records have increased from 25 to 26 bytes;
        the extra byte for the 2-byte status is inserted before the
        former status byte was found in the beginning of the structure;
        thus, the 1-byte status is now a short int 2-byte variable
        (MSB format like all other data except those in the 29-byte header)
    """
    # byte 1: number of bytes in Licor 7200 record (2 = missing, 25 = available)
    # B...unsigned char, integer, 1 Byte
    # h...short integer, 2 Bytes
    # >...big-endian, MSB at lowest address

    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]  # col 1/13
            status_code = unpacked_data[1]  # col 2/13
            if data_size == 26:  # if no missing data
                # read next 24 bytes for a total of 26 bytes / byte 3 is OCTAL, todo correct output for octal
                byte = open_file_object.read(24)
                if len(byte) == 24:  # check if we really have 24 more bytes

                    s = struct.Struct('>h B B B B B B B B B B B B B B B B B B B B B B')
                    unpacked_data = s.unpack(byte)

                    # DIAGNOSTIC VALUE
                    # Li72 manual: "The cell diagnostic value is a 2 byte unsigned integer (value between 0 and 8191)"
                    diag_val = unpacked_data[0]  # diagnostic value, MSB!  # col 3/13

                    # extract info from diag_val; for the output, we use only the diag_val integer value
                    # exception: signal strength is part of the default output
                    diag_val_bin = bin(diag_val)[2:].zfill(16)

                    # bits 13-15: always read 0
                    unused = diag_val_bin[0:3]

                    # bit 12:   sensor head attached to LI-7550; 1 = LI-7200
                    head_detect = diag_val_bin[3:4]

                    # bit 11:   1 = thermocouple OK; 0 = thermocouple open circuit
                    Toutlet = diag_val_bin[4:5]

                    # bit 10:   1 = thermocouple OK; 0 = thermocouple open circuit
                    Tintlet = diag_val_bin[5:6]

                    # bit 9:    1 = internal reference voltages OK;
                    #           0 = internal reference voltages not OK, analyzer interface unit needs service
                    aux_input = diag_val_bin[6:7]

                    # bit 8:    1 = good, 0.1 to 4.9V; 0 = out of range; d=delta
                    dpressure = diag_val_bin[7:8]

                    # bit 7:    1 = chopper wheel temp is near setpoint;
                    #           0 = not near setpoint
                    chopper = diag_val_bin[8:9]

                    # bit 6:    1 = detector temp is near setpoint
                    #           0 = not near setpoint
                    detector = diag_val_bin[9:10]

                    # bit 5:    1 = OK; lock bit, indicates that optical wheel is rotating at the correct rate
                    PLL = diag_val_bin[10:11]

                    # bit 4:    always set to 1 (OK)
                    sync = diag_val_bin[11:12]

                    # bits 0-3: value x 6.67 = signal strength
                    signal_strength = diag_val_bin[12:16]
                    signal_strength = int(str(signal_strength), 2)  # convert bin to int
                    signal_strength = (signal_strength * 6.67)  # col 4/13

                    # dry mole fraction, mmol mol-1; col 5/13
                    h2o_ppt = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[3])

                    # dry mole fraction, umol mol-1; col 6/13
                    co2_ppm = 0.0001 * ((unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[6])

                    # col 7/13
                    h2o_mmol_m3 = 0.001 * ((unpacked_data[7] * 256 * 256) + (unpacked_data[8] * 256) + unpacked_data[9])

                    # col 8/13
                    co2_mmol_m3 = 0.0001 * (
                            (unpacked_data[10] * 256 * 256) + (unpacked_data[11] * 256) + unpacked_data[12])

                    # offset; col 9/13
                    cell_temp_in_deg_c = 0.01 * ((unpacked_data[13] * 256) + unpacked_data[14])
                    cell_temp_in_deg_c -= 100

                    # to hPa; col 10/13
                    cell_press_hpa = 10 * ((unpacked_data[15] * 256) + unpacked_data[16])  # Pa
                    cell_press_hpa *= 0.01

                    # to hPa; col 11/13
                    atm_press_hpa = 10 * ((unpacked_data[17] * 256) + unpacked_data[18])  # Pa
                    atm_press_hpa *= 0.01

                    # col 12/13
                    cooler_v = 0.001 * ((unpacked_data[19] * 256) + (unpacked_data[20]))

                    # col 13/13
                    flow_l_min = 0.001 * ((unpacked_data[21] * 256) + (unpacked_data[22]))

                    return data_size, status_code, diag_val, signal_strength, h2o_ppt, co2_ppm, h2o_mmol_m3, \
                           co2_mmol_m3, cell_temp_in_deg_c, cell_press_hpa, atm_press_hpa, cooler_v, flow_l_min
                else:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

            else:
                # -----------------------------------------------
                # this part was included in v 0.86
                # necessary b/c in CH-DAV starting in Oct 2018 sometimes a 16 Byte data_size is detected
                # the exact reason for this is unknown, but it seems like the sonicread script tries
                # to store an IRGA75 data block (but IRGA75 is not installed at the site)
                # therefore, in this part here, we look at the data size that is given in the file (e.g. 16 Bytes),
                # then read that given number of Bytes minus 2 bytes (b/c the first two Bytes were already read)
                # from the file, and then output the normal missing data header.
                # a similar case was reported above for the IRGA75 (see description in data block).
                # -----------------------------------------------

                if data_size == 2:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size > 0:
                    # in case the data size is different than the typical 2 or 26 Bytes, the given number
                    # of Bytes is read from the file and the regular header is output
                    for xx in range(0, data_size - 2):
                        byte = open_file_object.read(1)
                        if len(byte) == 1:  # 2015-04-24
                            s = struct.Struct('B')
                            unpacked_data = s.unpack(byte)
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

                elif data_size == 0:
                    print("!ATTENTION! IRGA LI-7200 data size is not 2 or 26 bytes. Found data_size = 0.")
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_irga_li7200_extended_co2_gain0974(open_file_object):  # size: 12 columns / 25 bytes
    """Same as data_block_irga_li7200_extended, but with an additional gain applied to
    the CO2 data columns. Gain is necessary in some years when a wrong calibration gas
    was used to calibrate the LI-7200.

    Instrument ID in EddyPro:
        li-7200_extended_co2_gain0974

    Data block added in v0.94

    More info here:
    https://www.swissfluxnet.ethz.ch/index.php/knowledge-base/wrong-calibration-gas-2017/
    """
    calib_gas_corr_gain = 0.974
    # byte 1: number of bytes in Licor 7200 record (2 = missing, 25 = available)
    # B...unsigned char, integer, 1 Byte
    # h...short integer, 2 Bytes
    # >...big-endian, MSB at lowest address

    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]  # col 1/13
            status_code = unpacked_data[1]  # col 2/13
            if data_size == 26:  # if no missing data
                # read next 24 bytes for a total of 26 bytes / byte 3 is OCTAL, todo correct output for octal
                byte = open_file_object.read(24)
                if len(byte) == 24:  # check if we really have 24 more bytes

                    s = struct.Struct('>h B B B B B B B B B B B B B B B B B B B B B B')
                    unpacked_data = s.unpack(byte)

                    # DIAGNOSTIC VALUE
                    # Li72 manual: "The cell diagnostic value is a 2 byte unsigned integer (value between 0 and 8191)"
                    diag_val = unpacked_data[0]  # diagnostic value, MSB!  # col 3/13

                    # extract info from diag_val; for the output, we use only the diag_val integer value
                    # exception: signal strength is part of the default output
                    diag_val_bin = bin(diag_val)[2:].zfill(16)

                    # bits 13-15: always read 0
                    unused = diag_val_bin[0:3]

                    # bit 12:   sensor head attached to LI-7550; 1 = LI-7200
                    head_detect = diag_val_bin[3:4]

                    # bit 11:   1 = thermocouple OK; 0 = thermocouple open circuit
                    Toutlet = diag_val_bin[4:5]

                    # bit 10:   1 = thermocouple OK; 0 = thermocouple open circuit
                    Tintlet = diag_val_bin[5:6]

                    # bit 9:    1 = internal reference voltages OK;
                    #           0 = internal reference voltages not OK, analyzer interface unit needs service
                    aux_input = diag_val_bin[6:7]

                    # bit 8:    1 = good, 0.1 to 4.9V; 0 = out of range; d=delta
                    dpressure = diag_val_bin[7:8]

                    # bit 7:    1 = chopper wheel temp is near setpoint;
                    #           0 = not near setpoint
                    chopper = diag_val_bin[8:9]

                    # bit 6:    1 = detector temp is near setpoint
                    #           0 = not near setpoint
                    detector = diag_val_bin[9:10]

                    # bit 5:    1 = OK; lock bit, indicates that optical wheel is rotating at the correct rate
                    PLL = diag_val_bin[10:11]

                    # bit 4:    always set to 1 (OK)
                    sync = diag_val_bin[11:12]

                    # bits 0-3: value x 6.67 = signal strength
                    signal_strength = diag_val_bin[12:16]
                    signal_strength = int(str(signal_strength), 2)  # convert bin to int
                    signal_strength = (signal_strength * 6.67)  # col 4/13

                    # dry mole fraction, mmol mol-1; col 5/13
                    h2o_ppt = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[3])

                    # dry mole fraction, umol mol-1; col 6/13
                    co2_ppm = 0.0001 * ((unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[6])
                    co2_ppm = co2_ppm * calib_gas_corr_gain

                    # col 7/13
                    h2o_mmol_m3 = 0.001 * ((unpacked_data[7] * 256 * 256) + (unpacked_data[8] * 256) + unpacked_data[9])

                    # col 8/13
                    co2_mmol_m3 = 0.0001 * (
                            (unpacked_data[10] * 256 * 256) + (unpacked_data[11] * 256) + unpacked_data[12])
                    co2_mmol_m3 = co2_mmol_m3 * calib_gas_corr_gain

                    # offset; col 9/13
                    cell_temp_in_deg_c = 0.01 * ((unpacked_data[13] * 256) + unpacked_data[14])
                    cell_temp_in_deg_c -= 100

                    # to hPa; col 10/13
                    cell_press_hpa = 10 * ((unpacked_data[15] * 256) + unpacked_data[16])  # Pa
                    cell_press_hpa *= 0.01

                    # to hPa; col 11/13
                    atm_press_hpa = 10 * ((unpacked_data[17] * 256) + unpacked_data[18])  # Pa
                    atm_press_hpa *= 0.01

                    # col 12/13
                    cooler_v = 0.001 * ((unpacked_data[19] * 256) + (unpacked_data[20]))

                    # col 13/13
                    flow_l_min = 0.001 * ((unpacked_data[21] * 256) + (unpacked_data[22]))

                    return data_size, status_code, diag_val, signal_strength, h2o_ppt, co2_ppm, h2o_mmol_m3, \
                           co2_mmol_m3, cell_temp_in_deg_c, cell_press_hpa, atm_press_hpa, cooler_v, flow_l_min
                else:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

            else:
                # -----------------------------------------------
                # this part was included in v 0.86
                # necessary b/c in CH-DAV starting in Oct 2018 sometimes a 16 Byte data_size is detected
                # the exact reason for this is unknown, but it seems like the sonicread script tries
                # to store an IRGA75 data block (but IRGA75 is not installed at the site)
                # therefore, in this part here, we look at the data size that is given in the file (e.g. 16 Bytes),
                # then read that given number of Bytes minus 2 bytes (b/c the first two Bytes were already read)
                # from the file, and then output the normal missing data header.
                # a similar case was reported above for the IRGA75 (see description in data block).
                # -----------------------------------------------

                if data_size == 2:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
                elif data_size > 0:
                    # in case the data size is different than the typical 2 or 26 Bytes, the given number
                    # of Bytes is read from the file and the regular header is output
                    for xx in range(0, data_size - 2):
                        byte = open_file_object.read(1)
                        if len(byte) == 1:  # 2015-04-24
                            s = struct.Struct('B')
                            unpacked_data = s.unpack(byte)
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

                elif data_size == 0:
                    print("!ATTENTION! IRGA LI-7200 data size is not 2 or 26 bytes. Found data_size = 0.")
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_irga_li7200_co2_gain0974(open_file_object):  # size: 12 columns / 25 bytes
    """Apply gain 0.974 to CO2 concentration measurements to correct for the
    usage of a wrong calibration gas.

    Note that this data block corrects the *NOT* EXTENDED LI-7200 data stream.

    Instrument ID in EddyPro:
        li7200_co2_gain0974

    Data block added in v0.9.6

    For more info see here:
    https://www.swissfluxnet.ethz.ch/index.php/knowledge-base/wrong-calibration-gas-2017/
    """
    calib_gas_corr_gain = 0.974

    # byte 1: number of bytes in Licor 7200 record (2 = missing, 25 = available)
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]
            status_code = unpacked_data[1]
            if data_size == 25:  # if no missing data
                byte = open_file_object.read(
                    23)  # read next 23 bytes for a total of 25 bytes / byte 3 is OCTAL, todo correct output for octal
                if len(byte) == 23:  # check if we really have 23 more bytes
                    s = struct.Struct('B B B B B B B B B B B B B B B B B B B B B B B')
                    unpacked_data = s.unpack(byte)
                    status_byte = unpacked_data[
                        0]  # this is the AGC, after firmware update 100 = good, before update 100 = bad
                    binary = bin(status_byte)[2:].zfill(8)
                    lower_nibble = binary[4:]
                    decimal = int(str(lower_nibble), 2)
                    status_byte = (decimal * 6.25)  # + 6.25  # AGC, window dirtiness, offset not used in ETH Flux
                    h2o_ppt = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[
                        3])  # dry mole fraction, mmol mol-1

                    # CO2 mixing ratio (dry mole fraction), umol mol-1
                    co2_ppm = \
                        0.0001 * ((unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[6])
                    co2_ppm = co2_ppm * calib_gas_corr_gain

                    h2o_mmol_m3 = 0.001 * ((unpacked_data[7] * 256 * 256) + (unpacked_data[8] * 256) + unpacked_data[9])

                    # CO2 molar density, mmol m-3
                    co2_mmol_m3 = \
                        0.0001 * ((unpacked_data[10] * 256 * 256) + (unpacked_data[11] * 256) + unpacked_data[12])
                    co2_mmol_m3 = co2_mmol_m3 * calib_gas_corr_gain

                    temp_deg_c = 0.01 * ((unpacked_data[13] * 256) + unpacked_data[14])  # K
                    temp_deg_c -= 100  # K, offset
                    press_hpa = 10 * ((unpacked_data[15] * 256) + unpacked_data[16])  # Pa
                    press_hpa *= 0.01  # to hPa
                    atm_press_hpa = 10 * ((unpacked_data[17] * 256) + unpacked_data[18])  # Pa
                    atm_press_hpa *= 0.01  # to hPa
                    cooler_v = 0.001 * ((unpacked_data[19] * 256) + (unpacked_data[20]))
                    flow_l_min = 0.001 * ((unpacked_data[21] * 256) + (unpacked_data[22]))

                    return data_size, status_code, status_byte, h2o_ppt, co2_ppm, h2o_mmol_m3, co2_mmol_m3, temp_deg_c, press_hpa, atm_press_hpa, cooler_v, flow_l_min
                else:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

            else:
                # -----------------------------------------------
                # this part was included in v 0.86,
                # originally for li7200_extended but I also include this here for the "normal" li7200
                # necessary b/c in CH-DAV starting in Oct 2018 sometimes a 16 Byte data_size is detected
                # the exact reason for this is unknown, but it seems like the sonicread script tries
                # to store an IRGA75 data block (but IRGA75 is not installed at the site)
                # therefore, in this part here, we look at the data size that is given in the file (e.g. 16 Bytes),
                # then read that given number of Bytes minus 2 bytes (b/c the first two Bytes were already read)
                # from the file, and then output the normal missing data header.
                # a similar case was reported above for the IRGA75 (see description in data block).
                # -----------------------------------------------

                if data_size == 2:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999'

                elif data_size > 0:
                    # in case the data size is different than the typical 2 or 25 Bytes, the given number
                    # of Bytes is read from the file and the regular header is output
                    for xx in range(0, data_size - 2):
                        byte = open_file_object.read(1)
                        if len(byte) == 1:
                            s = struct.Struct('B')
                            unpacked_data = s.unpack(byte)
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999'

                elif data_size == 0:
                    print("!ATTENTION! IRGA LI-7200 data size is not 2 or 25 bytes. Found data_size = 0.")
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999'

    # There was one problem that could not be solved at the time the data acquisition was written
    # and which affects all data collected until this may (or may not) be solved in a future version: every
    # 20th record or so a ﬂow rate of 0.0 L min −1 is reported which leads to an erroneous CO 2 and H 2 O
    # concentration in the primary variable(the dry mole fraction) whereas the mole density is OK.


def data_block_irga_li7200(open_file_object):  # size: 12 columns / 25 bytes
    # print('------- Li7200 IRGA information')
    # byte 1: number of bytes in Licor 7200 record (2 = missing, 25 = available)
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]
            status_code = unpacked_data[1]
            if data_size == 25:  # if no missing data
                byte = open_file_object.read(
                    23)  # read next 23 bytes for a total of 25 bytes / byte 3 is OCTAL, todo correct output for octal
                if len(byte) == 23:  # check if we really have 23 more bytes
                    s = struct.Struct('B B B B B B B B B B B B B B B B B B B B B B B')
                    unpacked_data = s.unpack(byte)
                    status_byte = unpacked_data[
                        0]  # this is the AGC, after firmware update 100 = good, before update 100 = bad
                    binary = bin(status_byte)[2:].zfill(8)
                    lower_nibble = binary[4:]
                    decimal = int(str(lower_nibble), 2)
                    status_byte = (decimal * 6.25)  # + 6.25  # AGC, window dirtiness, offset not used in ETH Flux
                    h2o_ppt = 0.001 * ((unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) + unpacked_data[
                        3])  # dry mole fraction, mmol mol-1
                    co2_ppm = 0.0001 * ((unpacked_data[4] * 256 * 256) + (unpacked_data[5] * 256) + unpacked_data[
                        6])  # dry mole fraction, umol mol-1
                    h2o_mmol_m3 = 0.001 * ((unpacked_data[7] * 256 * 256) + (unpacked_data[8] * 256) + unpacked_data[9])
                    co2_mmol_m3 = 0.0001 * (
                            (unpacked_data[10] * 256 * 256) + (unpacked_data[11] * 256) + unpacked_data[12])
                    temp_deg_c = 0.01 * ((unpacked_data[13] * 256) + unpacked_data[14])  # K
                    temp_deg_c -= 100  # K, offset
                    press_hpa = 10 * ((unpacked_data[15] * 256) + unpacked_data[16])  # Pa
                    press_hpa *= 0.01  # to hPa
                    atm_press_hpa = 10 * ((unpacked_data[17] * 256) + unpacked_data[18])  # Pa
                    atm_press_hpa *= 0.01  # to hPa
                    cooler_v = 0.001 * ((unpacked_data[19] * 256) + (unpacked_data[20]))
                    flow_l_min = 0.001 * ((unpacked_data[21] * 256) + (unpacked_data[22]))

                    return data_size, status_code, status_byte, h2o_ppt, co2_ppm, h2o_mmol_m3, co2_mmol_m3, temp_deg_c, press_hpa, atm_press_hpa, cooler_v, flow_l_min
                else:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'

            else:
                # -----------------------------------------------
                # this part was included in v 0.86,
                # originally for li7200_extended but I also include this here for the "normal" li7200
                # necessary b/c in CH-DAV starting in Oct 2018 sometimes a 16 Byte data_size is detected
                # the exact reason for this is unknown, but it seems like the sonicread script tries
                # to store an IRGA75 data block (but IRGA75 is not installed at the site)
                # therefore, in this part here, we look at the data size that is given in the file (e.g. 16 Bytes),
                # then read that given number of Bytes minus 2 bytes (b/c the first two Bytes were already read)
                # from the file, and then output the normal missing data header.
                # a similar case was reported above for the IRGA75 (see description in data block).
                # -----------------------------------------------

                if data_size == 2:
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999'

                elif data_size > 0:
                    # in case the data size is different than the typical 2 or 25 Bytes, the given number
                    # of Bytes is read from the file and the regular header is output
                    for xx in range(0, data_size - 2):
                        byte = open_file_object.read(1)
                        if len(byte) == 1:
                            s = struct.Struct('B')
                            unpacked_data = s.unpack(byte)
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999'

                elif data_size == 0:
                    print("!ATTENTION! IRGA LI-7200 data size is not 2 or 25 bytes. Found data_size = 0.")
                    return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', \
                           '-9999', '-9999', '-9999', '-9999', '-9999'

    # There was one problem that could not be solved at the time the data acquisition was written
    # and which affects all data collected until this may (or may not) be solved in a future version: every
    # 20th record or so a ﬂow rate of 0.0 L min −1 is reported which leads to an erroneous CO 2 and H 2 O
    # concentration in the primary variable(the dry mole fraction) whereas the mole density is OK.


def data_block_qcl_ver1_var2(open_file_object):  # size: 26 bytes
    # this is the QCL version 1 variant 2 according to Werner Eugster's Table 2 in sonicread.pdf
    # this is a special variation of the typical QCL setup and was used at Laegeren
    # in May and June 2008
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        unpacked_data = s.unpack(byte)
        data_size = unpacked_data[0]
        status_code = unpacked_data[1]
        if data_size == 26:  # if no missing data
            byte = open_file_object.read(24)  # read next 12 bytes for a total of 14 bytes
            s = struct.Struct('>B B B B B B B B B B B B B B B B B B B B B B B B')  # MSB
            unpacked_data = s.unpack(byte)

            dummy1 = ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) +
                      unpacked_data[3])
            dummy2 = ((unpacked_data[4] * 256 * 256 * 256) + (unpacked_data[5] * 256 * 256) + (unpacked_data[6] * 256) +
                      unpacked_data[7])
            dummy3 = ((unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                    unpacked_data[10] * 256) + unpacked_data[11])
            dummy4 = ((unpacked_data[12] * 256 * 256 * 256) + (unpacked_data[13] * 256 * 256) + (
                    unpacked_data[14] * 256) + unpacked_data[15])
            dummy5 = ((unpacked_data[16] * 256 * 256 * 256) + (unpacked_data[17] * 256 * 256) + (
                    unpacked_data[18] * 256) + unpacked_data[19])
            dummy6 = ((unpacked_data[20] * 256 * 256 * 256) + (unpacked_data[21] * 256 * 256) + (
                    unpacked_data[22] * 256) + unpacked_data[23])

            return data_size, status_code, dummy1, dummy2, dummy3, dummy4, dummy5, dummy6

        else:
            return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_qcl_ver1_var0(open_file_object):  # size: 16 bytes
    # this is the QCL version 1 variant 0 according to Werner Eugster's Table 2 in sonicread.pdf
    # this was used at LAE 2005-09-30 11:00 until 2005-11-12 11:00
    # Raw binary data contains an extra 2 Bytes between 2005-09-30 11:00 and 2005-10-11 11:00:
    # during this time the status byte says data is 16 bytes (which is correct), but data are only stored
    # in 12 Bytes. Therefore, in that case we read 16 Bytes, but use only the first 12 Bytes after
    # the 2-Byte header for the data output.
    # Either way, the structure in both cases (16 or 14 Bytes length) seems to be the same.
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        unpacked_data = s.unpack(byte)
        data_size = unpacked_data[0]
        status_code = unpacked_data[1]
        if data_size == 16 or data_size == 14:  # if no missing data
            if data_size == 16:
                byte = open_file_object.read(14)  # read next 14 bytes for a total of 16 bytes
                s = struct.Struct('>B B B B B B B B B B B B B B')  # MSB
            elif data_size == 14:
                byte = open_file_object.read(12)  # read next 12 bytes for a total of 14 bytes
                s = struct.Struct('>B B B B B B B B B B B B')  # MSB
            unpacked_data = s.unpack(byte)

            # use only the first 12 Bytes for data output
            # this corresponds to N2O, CO2, H2O
            dummy1 = ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (unpacked_data[2] * 256) +
                      unpacked_data[3])
            dummy2 = ((unpacked_data[4] * 256 * 256 * 256) + (unpacked_data[5] * 256 * 256) + (unpacked_data[6] * 256) +
                      unpacked_data[7])
            dummy3 = ((unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                    unpacked_data[10] * 256) + unpacked_data[11])

            return data_size, status_code, dummy1, dummy2, dummy3

        else:
            return data_size, status_code, '-9999', '-9999', '-9999'


def data_block_qcl_ver1_var5(open_file_object):  # size: 14 bytes
    # this is the QCL version 1 variant 5 according to Werner Eugster's Table 2 in sonicread.pdf
    # this was used at CHA from 2009-08-28 19:51 until 2009-11-03 05:01
    # and again from 2010-03-11 13:18 until 2010-04-28 05:01
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        unpacked_data = s.unpack(byte)
        data_size = unpacked_data[0]
        status_code = unpacked_data[1]
        if data_size == 18:  # if no missing data
            byte = open_file_object.read(16)  # read next 16 bytes for a total of 18 bytes
            if len(byte) == 16:
                s = struct.Struct('>B B B B B B B B B B B B B B B B')  # MSB
                unpacked_data = s.unpack(byte)
                n2o = 0.001 * ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (
                        unpacked_data[2] * 256) + unpacked_data[3])
                ch4 = 0.001 * ((unpacked_data[4] * 256 * 256 * 256) + (unpacked_data[5] * 256 * 256) + (
                        unpacked_data[6] * 256) + unpacked_data[7])
                no2 = 0.001 * ((unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                        unpacked_data[10] * 256) + unpacked_data[11])
                h2o = ((unpacked_data[12] * 256 * 256 * 256) + (unpacked_data[13] * 256 * 256) + (
                        unpacked_data[14] * 256) + unpacked_data[15])

                return data_size, status_code, ch4, n2o, no2, h2o

        else:
            return data_size, status_code, '-9999', '-9999', '-9999', '-9999'


def data_block_qcl_ver1_var6(open_file_object):  # size: 14 bytes
    # this is the QCL version 1 variant 6 according to Werner Eugster's Table 2 in sonicread.pdf
    # this was used at CHA from 2012-01-17 23:21 (ongoing at the time of writing)
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        unpacked_data = s.unpack(byte)
        data_size = unpacked_data[0]
        status_code = unpacked_data[1]
        if data_size == 14:  # if no missing data
            byte = open_file_object.read(12)  # read next 12 bytes for a total of 14 bytes
            if len(byte) == 12:
                s = struct.Struct('>B B B B B B B B B B B B')  # MSB
                unpacked_data = s.unpack(byte)
                ch4 = 0.001 * ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (
                        unpacked_data[2] * 256) + unpacked_data[3])
                n2o = 0.001 * ((unpacked_data[4] * 256 * 256 * 256) + (unpacked_data[5] * 256 * 256) + (
                        unpacked_data[6] * 256) + unpacked_data[7])
                h2o = ((unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                        unpacked_data[10] * 256) + unpacked_data[11])

                return data_size, status_code, ch4, n2o, h2o

        else:
            return data_size, status_code, '-9999', '-9999', '-9999'


def data_block_qcl_ver1_var6_b(open_file_object):  # size: 22 bytes 14
    # Modified data acquisition to also record QCL temperature and pressure.
    # Now there are 5 QCL variables that are detected (there were 3 in previous versions).
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        unpacked_data = s.unpack(byte)
        data_size = unpacked_data[0]
        status_code = unpacked_data[1]
        if data_size == 22:  # if no missing data
            byte = open_file_object.read(20)  # read next 20 bytes for a total of 22 bytes
            if len(byte) == 20:
                s = struct.Struct('>B B B B B B B B B B B B B B B B B B B B')  # MSB
                unpacked_data = s.unpack(byte)
                ch4 = 0.001 * ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (
                        unpacked_data[2] * 256) + unpacked_data[3])  # 4 Bytes
                n2o = 0.001 * ((unpacked_data[4] * 256 * 256 * 256) + (unpacked_data[5] * 256 * 256) + (
                        unpacked_data[6] * 256) + unpacked_data[7])  # 4 Bytes
                h2o = ((unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                        unpacked_data[10] * 256) + unpacked_data[11])  # 4 Bytes
                cell_temperature = ((unpacked_data[12] * 256 * 256 * 256) + (unpacked_data[13] * 256 * 256) + (
                        unpacked_data[14] * 256) + unpacked_data[15]) / 1000  # 4 Bytes, in Kelvin
                cell_pressure = ((unpacked_data[16] * 256 * 256 * 256) + (unpacked_data[17] * 256 * 256) + (
                        unpacked_data[18] * 256) + unpacked_data[19]) / 1000  # 4 Bytes, in Torr

                return data_size, status_code, ch4, n2o, h2o, cell_temperature, cell_pressure

        else:
            return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_qcl_ver1_var6_b_ch_cha_2018_2(open_file_object):  # size: 22 bytes 14
    # added in v 0.87, 2019-02-28, see also notes
    # used in CH-CHA 2018_2
    # different column order and N2O (x10000), CH4 (x10) and cell temperature (x10) are amplified
    # also cell pressure is missing and H2O values are cut-off after two digites after the comma
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        unpacked_data = s.unpack(byte)
        data_size = unpacked_data[0]
        status_code = unpacked_data[1]
        if data_size == 22:  # if no missing data
            byte = open_file_object.read(20)  # read next 20 bytes for a total of 22 bytes
            if len(byte) == 20:
                s = struct.Struct('>B B B B B B B B B B B B B B B B B B B B')  # MSB
                unpacked_data = s.unpack(byte)

                # CH4: 4 Bytes (amplified by 10 for a total of 10000)
                ch4 = 0.0001 * ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (
                        unpacked_data[2] * 256) + unpacked_data[3])

                # H2O: 4 Bytes (only 2 digits after comma), need gain 1000 to five proper ppb values
                h2o = 1000 * ((unpacked_data[4] * 256 * 256 * 256) + (unpacked_data[5] * 256 * 256) + (
                        unpacked_data[6] * 256) + unpacked_data[7])

                # N2O: 4 Bytes (amplified by 10 for a total of 10000)
                n2o = 0.0001 * ((unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                        unpacked_data[10] * 256) + unpacked_data[11])

                # DUMMY: 4 Bytes
                dummy = 1 * ((unpacked_data[12] * 256 * 256 * 256) + (unpacked_data[13] * 256 * 256) + (
                        unpacked_data[14] * 256) + unpacked_data[15])

                # CELL TEMP: 4 Bytes, in Kelvin (amplified by 10 for a total of 10000)
                cell_temperature = 0.0001 * ((unpacked_data[16] * 256 * 256 * 256) + (unpacked_data[17] * 256 * 256) + (
                        unpacked_data[18] * 256) + unpacked_data[19])

                return data_size, status_code, ch4, h2o, n2o, dummy, cell_temperature

        else:
            return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_qcl_dav2015(open_file_object):  # size: 34
    # used in CH-DAV starting 2015-11-14 ongoing
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        unpacked_data = s.unpack(byte)
        data_size = unpacked_data[0]
        status_code = unpacked_data[1]
        if data_size == 34:  # if no missing data
            byte = open_file_object.read(32)  # read next 32 bytes for a total of 34 bytes
            if len(byte) == 32:
                s = struct.Struct('>B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B')  # MSB
                unpacked_data = s.unpack(byte)
                ch4 = 0.001 * ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (
                        unpacked_data[2] * 256) + unpacked_data[3])  # 4 Bytes
                n2o = 0.001 * ((unpacked_data[4] * 256 * 256 * 256) + (unpacked_data[5] * 256 * 256) + (
                        unpacked_data[6] * 256) + unpacked_data[7])  # 4 Bytes
                co2 = ((unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                        unpacked_data[10] * 256) + unpacked_data[11])  # 4 Bytes
                h2o = ((unpacked_data[12] * 256 * 256 * 256) + (unpacked_data[13] * 256 * 256) + (
                        unpacked_data[14] * 256) + unpacked_data[15])  # 4 Bytes
                cell_temperature = ((unpacked_data[16] * 256 * 256 * 256) + (unpacked_data[17] * 256 * 256) + (
                        unpacked_data[18] * 256) + unpacked_data[19]) / 1000  # 4 Bytes, in Kelvin
                cell_pressure = ((unpacked_data[20] * 256 * 256 * 256) + (unpacked_data[21] * 256 * 256) + (
                        unpacked_data[22] * 256) + unpacked_data[23]) / 1000  # 4 Bytes, in Torr
                status_word = ((unpacked_data[24] * 256 * 256 * 256) + (unpacked_data[25] * 256 * 256) + (
                        unpacked_data[26] * 256) + unpacked_data[27]) / 1000  # 4 Bytes, in Torr
                vici = ((unpacked_data[28] * 256 * 256 * 256) + (unpacked_data[29] * 256 * 256) + (
                        unpacked_data[30] * 256) + unpacked_data[31]) / 1000  # 4 Bytes, in Torr

                return data_size, status_code, ch4, n2o, co2, h2o, cell_temperature, cell_pressure, status_word, vici

        else:
            return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_lgr_ino2018(open_file_object):  # size: 12 columns / 33 bytes
    # this is the LGR-FMA extension version 1 variant 7, described in Table 14 of Werner Eugster's sonicread.pdf
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]  # Column 1, Byte (B) 1
            status_code = unpacked_data[1]  # Col 2, B 2
            if data_size == 33:  # if no missing data
                byte = open_file_object.read(31)  # read next 31 bytes for a total of 33 bytes
                if len(byte) == 31:
                    s = struct.Struct('>B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B')  # MSB
                    unpacked_data = s.unpack(byte)

                    # CH4 dry mole fraction, ppm x 10,000,000, Col 3, 4B: 3-6
                    ch4_dmf = \
                        10 ** -7 * (
                                (unpacked_data[0] * 256 * 256 * 256) +
                                (unpacked_data[1] * 256 * 256) +
                                (unpacked_data[2] * 256) +
                                unpacked_data[3])

                    # N2O dry mole fraction, ppm x 10,000,000, Col 4, 4B: 7-10
                    n2o_dmf = \
                        10 ** -7 * (
                                (unpacked_data[4] * 256 * 256 * 256) +
                                (unpacked_data[5] * 256 * 256) +
                                (unpacked_data[6] * 256) +
                                unpacked_data[7])

                    # H2O concentration, ppm x 10,000, Col 5, 4B: 11-14
                    h2o_conc = \
                        10 ** -4 * (
                                (unpacked_data[8] * 256 * 256 * 256) +
                                (unpacked_data[9] * 256 * 256) +
                                (unpacked_data[10] * 256) +
                                unpacked_data[11])

                    # CH4 concentration in moist air, ppm x 10,000,000, Col 6, 4B: 15-18
                    ch4_conc = \
                        10 ** -7 * (
                                (unpacked_data[12] * 256 * 256 * 256) +
                                (unpacked_data[13] * 256 * 256) +
                                (unpacked_data[14] * 256) +
                                unpacked_data[15])

                    # N2O concentration in moist air, ppm x 10,000,000, Col 7, 4B: 19-22
                    n2o_conc = \
                        10 ** -7 * (
                                (unpacked_data[16] * 256 * 256 * 256) +
                                (unpacked_data[17] * 256 * 256) +
                                (unpacked_data[18] * 256) +
                                unpacked_data[19])

                    # cell pressure, Torr x 100, Col 8, 2B: 23-24
                    cell_pressure = \
                        0.01 * (
                                (unpacked_data[20] * 256) +
                                unpacked_data[21])

                    # cell temperature, °C x 100, Col 9, 2B: 25-26
                    cell_temp = \
                        0.01 * (
                                (unpacked_data[22] * 256) +
                                unpacked_data[23])

                    # ambient temperature, °C x 100, Col 10, 2B: 27-28
                    ambient_temp = \
                        0.01 * (
                                (unpacked_data[24] * 256) +
                                unpacked_data[25])

                    # mirror ringdown time, us x 1,000,000, Col 11, 4B: 29-32
                    mirror_ringdown_time = \
                        10 ** -6 * (
                                (unpacked_data[26] * 256 * 256 * 256) +
                                (unpacked_data[27] * 256 * 256) +
                                (unpacked_data[28] * 256) +
                                unpacked_data[29])

                    # fit flag, integer 0-3, Col 12, 1B: 33
                    fit_flag = \
                        1 * (unpacked_data[30])

                    return data_size, status_code, \
                           ch4_dmf, n2o_dmf, h2o_conc, ch4_conc, n2o_conc, \
                           cell_pressure, cell_temp, ambient_temp, mirror_ringdown_time, fit_flag

            else:
                return data_size, status_code, \
                       '-9999', '-9999', '-9999', '-9999', '-9999', \
                       '-9999', '-9999', '-9999', '-9999', '-9999'


def data_block_lgr_fma(open_file_object):  # size: 7 columns / 15 bytes
    # this is the LGR-FMA extension version 1 variant 3, described in Table 9 of Werner Eugster's sonicread.pdf
    byte = open_file_object.read(2)
    if byte:  # if there are data to read
        s = struct.Struct('B B')
        if len(byte) == 2:
            unpacked_data = s.unpack(byte)
            data_size = unpacked_data[0]
            status_code = unpacked_data[1]
            if data_size == 15:  # if no missing data
                byte = open_file_object.read(13)  # read next 13 bytes for a total of 15 bytes
                if len(byte) == 13:
                    s = struct.Struct('>B B B B B B B B B B B B B')  # MSB
                    unpacked_data = s.unpack(byte)
                    ch4 = 0.0001 * ((unpacked_data[0] * 256 * 256 * 256) + (unpacked_data[1] * 256 * 256) + (
                            unpacked_data[2] * 256) + unpacked_data[3])  # 4 Bytes; ppb
                    cell_pressure = 0.01 * ((unpacked_data[4] * 256) + unpacked_data[5])  # 2 Bytes; Torr
                    cell_temperature = 0.01 * ((unpacked_data[6] * 256) + unpacked_data[7])  # 2 Bytes; °C
                    mirror_ringdown_time = 0.000001 * (
                            (unpacked_data[8] * 256 * 256 * 256) + (unpacked_data[9] * 256 * 256) + (
                            unpacked_data[10] * 256) + unpacked_data[11])  # 4 Bytes; us
                    calibration_flag = 1 * unpacked_data[12]

                    return data_size, status_code, ch4, cell_pressure, cell_temperature, mirror_ringdown_time, calibration_flag

            else:
                return data_size, status_code, '-9999', '-9999', '-9999', '-9999', '-9999'
        # else:
        #     return '-9999', '-9999', '-9999', '-9999', '-9999', '-9999', '-9999'
