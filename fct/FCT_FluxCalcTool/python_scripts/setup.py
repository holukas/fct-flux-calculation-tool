import datetime as dt
import fileinput
import fnmatch
import os
import shutil
import sys


def read_settings(root_folder):
    # insert path for essentials file into processing.eddypro
    os.chdir(root_folder)

    try:
        with open('settings.txt') as input_file:
            for line in input_file:
                if line.startswith('convert_raw'):  # indicates instrument information
                    p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                    convert_raw = p_value
                elif line.startswith('site_to_process'):
                    p_name, p_value = line.strip().split('=')
                    site_to_process = p_value
                elif line.startswith('plot_raw'):  # indicates instrument information
                    p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                    plot_raw = p_value
                elif line.startswith('calc_fluxes'):  # indicates instrument information
                    p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                    calc_fluxes = p_value
                elif line.startswith('plot_full_output'):  # indicates instrument information
                    p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                    plot_full_output = p_value
                elif line.startswith('plot_diurnal_cycles'):  # indicates instrument information
                    p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                    plot_diurnal_cycles = p_value
                elif line.startswith('plot_availability'):  # indicates instrument information
                    p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                    plot_availability = p_value
                elif line.startswith('raw_binary_file_extension'):  # specifies raw binary file extension
                    p_name, p_value = line.strip().split('=')  # extract name (left of =) and value (right of =) of line
                    raw_binary_file_extension = p_value
                elif line.startswith('download_raw_binary_data_from_server'):
                    p_name, p_value = line.strip().split('=')  #
                    download_raw_binary_data_from_server = p_value
                # elif line.startswith('raw_binary_source_dir'):
                #     p_name, p_value = line.strip().split('=')
                #     raw_binary_source_dir = p_value
                elif line.startswith('raw_binary_min_filesize_bytes'):
                    p_name, p_value = line.strip().split('=')
                    raw_binary_min_filesize_bytes = p_value
                elif line.startswith('download_eddypro_settings_from_server'):
                    p_name, p_value = line.strip().split('=')
                    download_eddypro_settings_from_server = p_value
                elif line.startswith('fluxes_from'):
                    p_name, p_value = line.strip().split('=')
                    fluxes_from = p_value
                elif line.startswith('fluxes_until'):
                    p_name, p_value = line.strip().split('=')
                    fluxes_until = p_value
                elif line.startswith('eddypro_flux_settings_dir'):
                    p_name, p_value = line.strip().split('=')
                    eddypro_flux_settings_dir = p_value
                elif line.startswith('download_how_many_files_max'):
                    p_name, p_value = line.strip().split('=')
                    download_how_many_files_max = p_value
                elif line.startswith('AWS_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    AWS_raw_binary_source_dir = p_value
                elif line.startswith('CHA_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    CHA_raw_binary_source_dir = p_value
                elif line.startswith('DAV_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    DAV_raw_binary_source_dir = p_value
                elif line.startswith('FRU_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    FRU_raw_binary_source_dir = p_value
                elif line.startswith('LAE_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    LAE_raw_binary_source_dir = p_value
                elif line.startswith('LAS_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    LAS_raw_binary_source_dir = p_value
                elif line.startswith('OE2_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    OE2_raw_binary_source_dir = p_value
                elif line.startswith('TWE_raw_binary_source_dir'):
                    p_name, p_value = line.strip().split('=')
                    TWE_raw_binary_source_dir = p_value
                elif line.startswith('use_rds'):
                    p_name, p_value = line.strip().split('=')
                    use_rds = p_value
    except IOError:
        print("Error: the file 'settings.txt' is missing in the directory.")
        input("Press Enter to end program...")
        sys.exit()

    return convert_raw, plot_raw, calc_fluxes, plot_full_output, plot_diurnal_cycles, plot_availability, raw_binary_file_extension, download_raw_binary_data_from_server, raw_binary_min_filesize_bytes, download_eddypro_settings_from_server, fluxes_from, fluxes_until, eddypro_flux_settings_dir, download_how_many_files_max, site_to_process, AWS_raw_binary_source_dir, CHA_raw_binary_source_dir, DAV_raw_binary_source_dir, FRU_raw_binary_source_dir, LAE_raw_binary_source_dir, LAS_raw_binary_source_dir, OE2_raw_binary_source_dir, TWE_raw_binary_source_dir, use_rds


def detect_instruments(root_dir):  # uses the eddypro metadata file to detect instruments

    import os
    import fnmatch
    import sys

    # root_folder = os.getcwd()  # current working directory
    allfiles = os.listdir(root_dir)  # list all files in current working directory

    # create empty LISTS
    p_name_all = []
    p_value_all = []
    p_name_all_column = []

    found_instruments = ()  # create empty tuple, tuple of found instruments
    found_instruments_id = ()  # create empty tuple, tuple of id for each found instrument
    eddypro_processing_file_exists = 0  # 1 = yes, 0 = no
    eddypro_metadata_file_exists = 0  # 1 = yes, 0 = no
    file_duration = -9999

    for file in allfiles:  # go through all files in dir

        file = os.path.join(root_dir, file)

        if fnmatch.fnmatch(file, '*.eddypro'):  # if the file is the *.eddypro processing file
            eddypro_processing_file_exists = 1  # it exists!
            with open(file) as input_file:  # open the file
                for line in input_file:  # go through contents of the file line-by-line
                    # read the site name from this file:
                    if line.startswith('project_id='):
                        dummy, site_name = line.strip().split('=')
                        print("site name: " + site_name)
                        break

        if fnmatch.fnmatch(file, '*.metadata'):  # if the file is the *.metadata metadata file
            eddypro_metadata_file_exists = 1
            print("Reading instrument setup from file: " + file)
            # search file to get number of instruments
            with open(file) as input_file:
                for line in input_file:
                    if line.startswith('instr_'):  # indicates instrument information
                        p_name, p_value = line.strip().split(
                            '=')  # extract name (left of =) and value (right of =) of line
                        p_name_contents = p_name.strip().split('_')  # extract parts of p_value
                        p_name_all.append(p_name)  # add this instrument info to list (parameter name)
                        p_value_all.append(p_value)  # add this instrument info to list (parameter value)
                        p_name_all_column.append(p_name_contents[1])  # column 2 (= index 1) contains column number
                        if 'model' in p_name_contents:
                            if p_name_contents[1] == '1':
                                print("instrument found: " + p_value)
                                found_instruments = p_value,  # type: tuple
                            else:
                                print("instrument found: " + p_value)
                                found_instruments += p_value,
                        if 'id' in p_name_contents:
                            if p_name_contents[1] == '1':
                                print("id found: " + p_value)
                                found_instruments_id = p_value,  # type: tuple
                            else:
                                print("id found: " + p_value)
                                found_instruments_id += p_value,
                    if line.startswith("file_duration="):
                        p_name, p_value = line.strip().split(
                            '=')  # extract name (left of =) and value (right of =) of line
                        file_duration = p_value  # duration of one file in minutes

            number_of_instruments = list(map(int, p_name_all_column))
            number_of_instruments = max(number_of_instruments)

    # check if we have what we need, otherwise stop program
    if eddypro_processing_file_exists == 0:
        print("EddyPro *.eddypro file not found")
        print("program stopped!")
        sys.exit()
    elif eddypro_metadata_file_exists == 0:
        print("EddyPro *.metadata file not found")
        print("program stopped!")
        sys.exit()

    print("number of instruments: " + str(number_of_instruments))

    # reset variables
    data_block_order = list()  # setup empty list, later contains instrument data block names
    data_block_order_id = list()

    # check which instruments are available at this site
    # add found instruments to the list
    for x in range(0, number_of_instruments):
        if 'r3_50' in found_instruments[x]:  # EddyPro uses 'r3_50' to describe the sonic r3_50
            data_block_order.append('data_block_sonic_r3_50')
            data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)

        # Gill HS-50
        if 'hs_50' in found_instruments[x]:
            if found_instruments_id[x] == 'hs-50':
                data_block_order.append('data_block_sonic_hs_50')
                data_block_order_id.append(found_instruments_id[x])
            elif found_instruments_id[x] == 'hs-50_extended':
                data_block_order.append('data_block_sonic_hs_50_extended')
                data_block_order_id.append(found_instruments_id[x])
            elif found_instruments_id[x] == 'hs-50_extended_synctest':
                data_block_order.append('data_block_sonic_hs_50_extended_synctest')
                data_block_order_id.append(found_instruments_id[x])

        # Licor Li-7200
        if 'li7200' in found_instruments[x]:
            if found_instruments_id[x] == 'li-7200':
                data_block_order.append('data_block_irga_li7200')
                data_block_order_id.append(found_instruments_id[x])
            elif found_instruments_id[x] == 'li-7200_co2_gain0974':
                data_block_order.append('data_block_irga_li7200_co2_gain0974')
                data_block_order_id.append(found_instruments_id[x])

            elif found_instruments_id[x] == 'li-7200_extended':
                data_block_order.append('data_block_irga_li7200_extended')
                data_block_order_id.append(found_instruments_id[x])
            elif found_instruments_id[x] == 'li-7200_extended_co2_gain0974':
                data_block_order.append('data_block_irga_li7200_extended_co2_gain0974')
                data_block_order_id.append(found_instruments_id[x])

            elif found_instruments_id[x] == 'li-7200_extended_synctest':  # new in 0.87
                data_block_order.append('data_block_irga_li7200_extended_synctest')
                data_block_order_id.append(found_instruments_id[x])

        if 'li7500' in found_instruments[x]:
            if found_instruments_id[x] == 'li-7500':
                data_block_order.append('data_block_irga_li7500')
                data_block_order_id.append(found_instruments_id[x])
            elif found_instruments_id[x] == 'li-7500_co2_gain0974':
                data_block_order.append('data_block_irga_li7500_co2_gain0974')
                data_block_order_id.append(found_instruments_id[x])

        if 'hs_100' in found_instruments[x]:
            data_block_order.append('data_block_sonic_hs_100')
            data_block_order_id.append(found_instruments_id[x])
        if 'csat3' in found_instruments[x]:
            data_block_order.append('data_block_sonic_csat3')
            data_block_order_id.append(found_instruments_id[x])



        if 'r2_1' in found_instruments[x]:  # EddyPro uses 'r2_1' to describe the sonic r2_1
            # earlier versions of sonicreadHS recorded an extra Byte in the data record
            if found_instruments_id[x] == 'r2_1_with_extrabyte':
                data_block_order.append('data_block_sonic_r2_1_with_extrabyte')
                data_block_order_id.append(found_instruments_id[x])
            elif found_instruments_id[x] == 'r2_1_without_extrabyte':
                data_block_order.append('data_block_sonic_r2_1_without_extrabyte')
                data_block_order_id.append(found_instruments_id[x])

        if 'generic_closed_path' in found_instruments[x]:
            # some variation exists in the data output of used QCL instruments
            if found_instruments_id[
                x] == 'qcl_v1v2':  # special case of QCL data, version 1 variant 2 in Werner Eugster's Table 2 in sonicread.pdf
                data_block_order.append('data_block_qcl_ver1_var2')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            elif found_instruments_id[
                x] == 'qcl_v1v0':  # special case of QCL data, version 1 variant 0 in Werner Eugster's Table 2 in sonicread.pdf
                data_block_order.append('data_block_qcl_ver1_var0')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            elif found_instruments_id[
                x] == 'qcl_v1v5':  # special case of QCL data, version 1 variant 5 in Werner Eugster's Table 2 in sonicread.pdf
                data_block_order.append('data_block_qcl_ver1_var5')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            elif found_instruments_id[
                x] == 'qcl_v1v6':  # special case of QCL data, version 1 variant 6 in Werner Eugster's Table 2 in sonicread.pdf
                data_block_order.append('data_block_qcl_ver1_var6')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            elif found_instruments_id[x] == 'qcl_v1v6b':  # another variant, logs 2 additional columns, cell temperature and cell pressure in CH-CHA
                data_block_order.append('data_block_qcl_ver1_var6_b')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            elif found_instruments_id[x] == 'qcl_v1v6b_ch-cha_2018_2':  # v 0.87, see notes
                data_block_order.append('data_block_qcl_ver1_var6_b_ch_cha_2018_2')
                data_block_order_id.append(found_instruments_id[x])
            elif found_instruments_id[
                x] == 'qcl_dav2015':  # another variant, logs 2 additional columns, cell temperature and cell pressure in CH-CHA
                data_block_order.append('data_block_qcl_dav2015')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            elif found_instruments_id[x] == 'lgr_fma':  # the LGR-FMA is also a 'generic closed path' in EddyPro
                data_block_order.append('data_block_lgr_fma')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            elif found_instruments_id[x] == 'lgr_ino2018':  # the LGR-FMA is also a 'generic closed path' in EddyPro
                data_block_order.append('data_block_lgr_ino2018')
                data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)
            # else:
            #     data_block_order.append('data_block_qcl')
            #     data_block_order_id.append(found_instruments_id[x])  # store instrument id (needed for QCL variants)

    # output instrument sequence info
    print("\nSequence of incoming data:")
    for x in range(0, len(data_block_order)):
        print("#" + str(x + 1) + ": " + data_block_order[x])
    print("Raw binary data will be read in this order.")

    return site_name, data_block_order, data_block_order_id, file_duration


def folder_structure(site_name, convert_raw, calc_fluxes, root_dir, source_dir,
                     fluxes_from, fluxes_until, download_raw_binary_data_from_server, now_string, fluxes_from_datetime,
                     fluxes_until_datetime):
    # check operating system
    print("used operating system: " + os.name)

    # FOLDER STRUCTURE

    # folders needed for INPUT
    eddypro_app_folder = os.path.join(source_dir, "eddypro_app\\")

    fluxes_from_stripped = fluxes_from.replace(':', '').replace(' ', '').replace('-', '')
    fluxes_until_stripped = fluxes_until.replace(':', '').replace(' ', '').replace('-', '')
    site_now_string = site_name + "_" + str(fluxes_from_stripped) + "_until_" + str(
        fluxes_until_stripped) + "_ID" + now_string
    output_dir = os.path.join(root_dir, "EP-OUT_" + site_name + "_" + str(fluxes_from_stripped) + "_until_" + str(
        fluxes_until_stripped) + "_ID" + now_string + "\\")

    raw_csv_dir = os.path.join(output_dir,
                               "converted_raw_data_csv\\")  # folder where all converted csv are stored, if option is set

    if download_raw_binary_data_from_server == 1:
        raw_binary_dir = os.path.join(output_dir,
                                      "raw_binary_files\\")  # if raw binary files are downloaded from server, use raw binary files from this folder in output_dir
    else:
        raw_binary_dir = os.path.join(source_dir,
                                      "raw_binary_files\\")  # if NO raw binary files are downloaded from server, use raw binary files from this folder in root

    # folders needed for OUTPUT
    # plot folders:
    plot_dir = os.path.join(output_dir, "plots\\")
    raw_csv_plots_dir = os.path.join(plot_dir,
                                     "raw_data_csv_plots\\")  # folder where all converted csv plots are stored, if option is set
    full_output_plots_dir = os.path.join(plot_dir,
                                         "full_output_plots\\")  # folder where all converted csv plots are stored, if option is set
    diurnal_cycle_plots_dir = os.path.join(plot_dir,
                                           "diurnal_cycle_plots\\")  # folder where all converted csv plots are stored, if option is set
    data_availability_plots_dir = os.path.join(plot_dir,
                                               "data_availability_plot\\")  # folder where all converted csv plots are stored, if option is set

    # other output folders:
    info_dir = os.path.join(output_dir, "raw_data_info\\")
    eddypro_dir = os.path.join(output_dir, "eddypro\\")
    eddypro_bin_dir = os.path.join(eddypro_dir, "bin\\")  # folder for RP + FCC files
    eddypro_ini_dir = os.path.join(eddypro_dir, "ini\\")  # folder for processing files
    eddypro_results_dir = os.path.join(eddypro_dir, "results\\")  # folder for processing files

    # delete output folder if it already exists and create new one, together with the other folders
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        os.makedirs(raw_csv_dir)
        os.makedirs(plot_dir)
        os.makedirs(full_output_plots_dir)
        os.makedirs(diurnal_cycle_plots_dir)
        os.makedirs(data_availability_plots_dir)
        os.makedirs(raw_csv_plots_dir)
        os.makedirs(eddypro_dir)
        os.makedirs(info_dir)
        os.makedirs(eddypro_bin_dir)
        os.makedirs(eddypro_ini_dir)
        os.makedirs(eddypro_results_dir)
        if download_raw_binary_data_from_server == 1:
            os.makedirs(raw_binary_dir)

    # output folder structure
    print("ascii output folder: \n" + output_dir)
    print("|___" + raw_csv_dir)
    if download_raw_binary_data_from_server == 1:
        print("|___" + raw_binary_dir)
    print("|___" + plot_dir)
    print("    |___" + full_output_plots_dir)
    print("    |___" + full_output_plots_dir)
    print("    |___" + diurnal_cycle_plots_dir)
    print("    |___" + raw_csv_plots_dir)
    print("    |___" + data_availability_plots_dir)
    print("|___" + info_dir)
    print("|___" + eddypro_dir)
    print("    |___" + eddypro_bin_dir)
    print("    |___" + eddypro_ini_dir)
    print("    |___" + eddypro_results_dir)
    print("\n")

    # if the raw binary files were converted in a previous run
    # search for the raw_data_csv folder and use it
    if convert_raw == 0:
        print("-----------------------------------------------")
        print("\nSEARCHING FOR PREVIOUSLY CONVERTED RAW FILES")
        print("Scanning directory...")
        print("Found files and folders:")
        print(os.listdir(root_dir))
        print("\nSearching for files in the time periods between " + fluxes_from_datetime.strftime(
            "%Y-%m-%d") + " and " + fluxes_until_datetime.strftime("%Y-%m-%d") + " ...\n")

        found_folder = 0

        for root, dirs, files in os.walk(root_dir):
            if "\\converted_raw_data_csv" in root:
                all_files_in_this_folder = os.listdir(root)

                for file in all_files_in_this_folder:
                    if file.endswith('.csv') and len(file) == 16 and \
                            file[0:4].isdigit() and file[4:6].isdigit() and \
                            file[6:8].isdigit() and file[8:10].isdigit():

                        this_year = int(file[0:4])
                        this_month = int(file[4:6])
                        this_day = int(file[6:8])
                        this_datetime = dt.datetime(this_year, this_month, this_day, 0,
                                                    0)  # check if the found file falls into our selected time period
                        if fluxes_from_datetime <= this_datetime <= fluxes_until_datetime:
                            found_folder += 1
                            if found_folder > 1:
                                print("\n(!) Found newer folder, previously found folder will be ignored:")
                            print("Found file " + file + " with ending .csv and length " + str(len(file)) + "!")
                            print("Date of found file: " + this_datetime.strftime("%Y-%m-%d"))
                            print("Using previously converted files from this folder: " + root)
                            raw_csv_dir = root
                            break

        print("\n-----------------------------------------------")

    # here we set the raw_csv_dir to the folder of previously converted raw files
    if convert_raw == 0:
        raw_csv_dir = raw_csv_dir
        print("(!) Note that raw csv files will be used from a previous run in this folder: ")
        print(raw_csv_dir)
        print("\n")

    # now we do the same for a previous EddyPro full_output csv file
    if calc_fluxes == 0:
        print(os.listdir(root_dir))
        for root, dirs, files in os.walk(root_dir):
            if "\\results" in root:
                print("\nEddyPro results from previous run assumed in: " + root)
                eddypro_results_dir = root
                break

    if calc_fluxes == 0:
        eddypro_results_dir = eddypro_results_dir
        print("(!) Note that the EddyPro full_output file from a previous run will be used, folder: ")
        print(eddypro_results_dir)
        print("\n")

    # ----------------------|COPY RP FCC files to bin folder
    os.chdir(eddypro_app_folder)

    # search for eddypro file in the respective OS folder
    if os.name == "posix" or os.name == "linux2":  # if linux
        folder = "linux"
    elif os.name == "mac":  # if OS X
        folder = "mac"
    elif os.name == "nt":  # if Windows
        folder = "windows"

    search_folder = os.path.join(eddypro_app_folder, folder)
    allfiles = os.listdir(search_folder)

    if os.name == "posix" or os.name == "linux2":  # if linux
        for file in allfiles:
            if fnmatch.fnmatch(file, 'eddypro_rp.exe'):  # todo
                shutil.copy(os.path.join(search_folder, file), eddypro_bin_dir)
            elif fnmatch.fnmatch(file, 'eddypro_fcc.exe'):  # copy eddypro_fcc file to bin folder
                shutil.copy(os.path.join(search_folder, file), eddypro_bin_dir)

    elif os.name == "mac":  # if OS X
        for file in allfiles:
            if fnmatch.fnmatch(file, 'eddypro_rp.exe'):  # todo
                shutil.copy(os.path.join(search_folder, file), eddypro_bin_dir)
            elif fnmatch.fnmatch(file, 'eddypro_fcc.exe'):  # copy eddypro_fcc file to bin folder
                shutil.copy(os.path.join(search_folder, file), eddypro_bin_dir)

    elif os.name == "nt":  # if Windows
        for file in allfiles:
            if fnmatch.fnmatch(file, 'eddypro_rp.exe'):
                shutil.copy(os.path.join(search_folder, file), eddypro_bin_dir)
            elif fnmatch.fnmatch(file, 'eddypro_fcc.exe'):  # copy eddypro_fcc file to bin folder
                shutil.copy(os.path.join(search_folder, file), eddypro_bin_dir)
            elif fnmatch.fnmatch(file, '*.dll'):  # copy dll files (needed since EP7)
                shutil.copy(os.path.join(search_folder, file), eddypro_bin_dir)

    # ----------------------|PROCESSING.EDDYPRO
    # copy *.eddypro file to ini folder
    os.chdir(root_dir)
    allfiles = os.listdir(root_dir)
    for file in allfiles:
        if fnmatch.fnmatch(file, '*.eddypro'):
            print(file + " found")

            # *.eddypro file MUST be named processing.eddypro in ini folder
            src_dir = root_dir
            dst_dir = eddypro_ini_dir
            src_file = file
            shutil.copy(src_file, dst_dir)

            # copy and rename processing.eddypro
            dst_file = os.path.join(dst_dir, file)
            new_dst_file_name = os.path.join(dst_dir, 'processing.eddypro')
            os.rename(dst_file, new_dst_file_name)

            # fill in CORRECT PATHS in processing.eddypro
            # remove backslash of path because eddypro does not like it
            with open(new_dst_file_name) as input_file:
                for line in input_file:
                    if line.startswith('out_path='):
                        out_path_old = line
                    elif line.startswith('proj_file='):
                        metadata_path_old = line
                    elif line.startswith('file_name='):
                        processing_path_old = line
                    elif line.startswith('data_path='):
                        data_path_old = line
                    elif line.startswith('file_prototype='):
                        file_prototype_old = line

            out_path_new = 'out_path=' + eddypro_results_dir.replace('\\', '/')
            if out_path_new[-1:] == '/':
                out_path_new = out_path_new[:-1] + '\n'
            else:
                out_path_new += '\n'
            for line in fileinput.input(new_dst_file_name, inplace=True):
                print(line.replace(out_path_old, out_path_new), end='')

            processing_path_new = 'file_name=' + new_dst_file_name.replace('\\', '/')
            if processing_path_new[-1:] == '/':
                processing_path_new = processing_path_new[:-1] + '\n'
            else:
                processing_path_new += '\n'
            for line in fileinput.input(new_dst_file_name, inplace=True):
                print(line.replace(processing_path_old, processing_path_new), end='')

            data_path_new = 'data_path=' + raw_csv_dir.replace('\\', '/')
            if data_path_new[-1:] == '/':
                data_path_new = data_path_new[:-1] + '\n'
            else:
                data_path_new += '\n'
            for line in fileinput.input(new_dst_file_name, inplace=True):
                print(line.replace(data_path_old, data_path_new), end='')

            file_prototype_new = 'file_prototype=yyyymmddHHMM.csv'
            if file_prototype_new[-1:] == '/':
                file_prototype_new = file_prototype_new[:-1] + '\n'
            else:
                file_prototype_new += '\n'
            for line in fileinput.input(new_dst_file_name, inplace=True):
                print(line.replace(file_prototype_old, file_prototype_new), end='')

    # ----------------------|METADATA FILE
    # copy *.metadata file to ini folder
    for file in allfiles:
        if fnmatch.fnmatch(file, '*.metadata'):
            print(file + " found")
            shutil.copy(file, eddypro_ini_dir)

            metadata_path_new = 'proj_file=' + eddypro_ini_dir.replace('\\', '/') + file
            if metadata_path_new[-1:] == '/':
                metadata_path_new = metadata_path_new[:-1] + '\n'
            else:
                metadata_path_new += '\n'
            for line in fileinput.input(new_dst_file_name, inplace=True):
                print(line.replace(metadata_path_old, metadata_path_new), end='')

    return raw_csv_dir, info_dir, eddypro_bin_dir, eddypro_ini_dir, output_dir, eddypro_results_dir, \
           raw_binary_dir, raw_csv_plots_dir, full_output_plots_dir, diurnal_cycle_plots_dir, data_availability_plots_dir, site_now_string


def construct_header(data_block_order, data_block_order_id):
    header_csv = ()

    for x in range(0, len(data_block_order)):
        if 'r3_50' in data_block_order[x]:
            # data_block_order.append('data_block_sonic_r3_50')
            header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_K', 'inc_x_deg', 'inc_y_deg')

        if 'hs_100' in data_block_order[x]:
            header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_K', 'inc_x_deg', 'inc_y_deg')

        if 'csat3' in data_block_order[x]:
            header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_C', 'counter')  # , 'delimiters')

        # LI-7500
        if 'li7500' in data_block_order[x]:
            if data_block_order_id[x] == 'li-7500':
                header_csv += ('data_size_75', 'status_code_75', 'status_byte_75', 'H2O_mmol_m-3_75',
                               'CO2_mmol_m-3_75', 'temp_degC_75', 'press_hPa_75', 'cooler_V_75')
            elif data_block_order_id[x] == 'li-7500_co2_gain0974':
                # v0.9.6, but would also have been valid for v0.9.5, but columns are the same anyway: same as
                # 'li-7500', but gain of 0.974 is applied to CO2 columns due to usage of a  wrong calibration
                # gas between 2017-2019
                # https://www.swissfluxnet.ethz.ch/index.php/knowledge-base/wrong-calibration-gas-2017/
                header_csv += ('data_size_75', 'status_code_75', 'status_byte_75', 'H2O_mmol_m-3_75',
                               'CO2_mmol_m-3_75', 'temp_degC_75', 'press_hPa_75', 'cooler_V_75')

        # Gill HS-50
        if 'hs_50' in data_block_order[x]:
            if data_block_order_id[x] == 'hs-50':
                header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_K', 'inc_x_deg', 'inc_y_deg')
            elif data_block_order_id[x] == 'hs-50_extended':
                header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_K', 'StaA_SA', 'StaD_SA', 'inc_x_or_y_deg')
            elif data_block_order_id[x] == 'hs-50_extended_synctest':
                header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_K', 'X', 'X', 'StaA_SA', 'StaD_SA', 'inc_x_or_y_deg')

        # LI-7200
        if 'li7200' in data_block_order[x]:
            if data_block_order_id[x] == 'li-7200':
                header_csv += ('data_size_72', 'status_code_72', 'status_byte_72', 'H2O_ppt_72', 'CO2_ppm_72',
                               'H2O_mmol_m-3_72', 'CO2_mmol_m-3_72', 'temp_degC_72', 'press_hPa_72', 'atm_press_hPa_72',
                               'cooler_V_72', 'flow_l_min-1_72')
            elif data_block_order_id[x] == 'li-7200_co2_gain0974':
                # v0.9.6: same as 'li-7200', but gain of 0.974 is applied to CO2 columns due
                # to usage of a wrong calibration gas between 2017-2019
                # https://www.swissfluxnet.ethz.ch/index.php/knowledge-base/wrong-calibration-gas-2017/
                header_csv += ('data_size_72', 'status_code_72', 'status_byte_72', 'H2O_ppt_72', 'CO2_ppm_72',
                               'H2O_mmol_m-3_72', 'CO2_mmol_m-3_72', 'temp_degC_72', 'press_hPa_72', 'atm_press_hPa_72',
                               'cooler_V_72', 'flow_l_min-1_72')

            elif data_block_order_id[x] == 'li-7200_extended':
                header_csv += (
                'data_size_72', 'status_code_72', 'diagnostic_value_72', 'signal_strength_72', 'H2O_ppt_72',
                'CO2_ppm_72', 'H2O_mmol_m-3_72', 'CO2_mmol_m-3_72', 'cell_temp_in_degC_72', 'cell_press_hPa_72',
                'ambient_press_hPa_72', 'cooler_V_72', 'flow_l_min-1_72')

            elif data_block_order_id[x] == 'li-7200_extended_co2_gain0974':
                # v0.94: same as 'li-7200_extended', but gain of 0.974 is applied to CO2 columns due
                # to usage of a  wrong calibration gas between 2017-2019
                # https://www.swissfluxnet.ethz.ch/index.php/knowledge-base/wrong-calibration-gas-2017/
                header_csv += (
                'data_size_72', 'status_code_72', 'diagnostic_value_72', 'signal_strength_72', 'H2O_ppt_72',
                'CO2_ppm_72', 'H2O_mmol_m-3_72', 'CO2_mmol_m-3_72', 'cell_temp_in_degC_72', 'cell_press_hPa_72',
                'ambient_press_hPa_72', 'cooler_V_72', 'flow_l_min-1_72')

            elif data_block_order_id[x] == 'li-7200_extended_synctest':
                header_csv += (
                'data_size_72', 'status_code_72', 'diagnostic_value_72', 'cooler_V_72', 'AUX3',
                'H2O_mmol_m-3_72', 'CO2_mmol_m-3_72', 'cell_temp_in_degC_72', 'cell_press_hPa_72',
                'ambient_press_hPa_72', 'AUX1', 'H2O_ppt_72',

                'signal_strength_72',
                'CO2_ppm_72',
                 'cooler_V_72', 'flow_l_min-1_72')

        #         DiagWord, Cooler, AUX3, H2OD, CO2D, Temp, Pres, Apres, AUX1, H2OMFD

        if 'r2_1' in data_block_order[x]:
            # data_block_order.append('data_block_sonic_r2_1')
            if data_block_order_id[x] == 'r2_1_with_extrabyte':  # WITH extrabyte and WITH inc_x and inc_y
                header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_C', 'inc_x_deg', 'inc_y_deg')
            if data_block_order_id[x] == 'r2_1_without_extrabyte':  # without extrabyte and without inc_x and inc_y
                header_csv = ('u_ms-1', 'v_ms-1', 'w_ms-1', 'ts_C')

        if 'qcl' in data_block_order[x]:
            # For version and variant explanation please see Werner Eugster's Table 2 in sonicread.pdf
            if data_block_order_id[x] == 'qcl_v1v2':  # version 1 variant 2
                header_csv += (
                    'data_size_qcl', 'status_code_qcl', 'dummy1', 'dummy2', 'dummy3', 'dummy4', 'dummy5', 'dummy6')
            if data_block_order_id[x] == 'qcl_v1v0':  # EMPA format, e.g. LAE 2005-09-30 11:00 until 2005-11-12 11:00
                header_csv += (
                    'data_size_qcl', 'status_code_qcl', 'dummy1', 'dummy2', 'dummy3')
            if data_block_order_id[x] == 'qcl_v1v5':  # CHA 2009 / 2010
                header_csv += (
                'data_size_qcl', 'status_code_qcl', 'n2o_ppb_qcl', 'ch4_ppb_qcl', 'no2_qcl', 'h2o_ppb_qcl')
            if data_block_order_id[x] == 'qcl_v1v6':  # CHA from 2012-01-17 23:21
                header_csv += ('data_size_qcl', 'status_code_qcl', 'ch4_ppb_qcl', 'n2o_ppb_qcl', 'h2o_ppb_qcl')
            if data_block_order_id[x] == 'qcl_v1v6b':  # CHA from 2015-05-08 ongoing todo check if date correct
                header_csv += ('data_size_qcl', 'status_code_qcl', 'ch4_ppb_qcl', 'n2o_ppb_qcl', 'h2o_ppb_qcl',
                               'cell_temp_K_qcl', 'cell_press_torr_qcl')
            if data_block_order_id[x] == 'qcl_v1v6b_ch-cha_2018_2':
                # v 0.87: CHA in 2018_2, different order, cell pressure missing, CH4 and N2O
                header_csv += ('data_size_qcl', 'status_code_qcl', 'ch4_ppb_qcl', 'h2o_ppb_qcl',
                               'n2o_ppb_qcl', '_dummy_qcl', 'cell_temp_K_qcl',)
            if data_block_order_id[x] == 'qcl_dav2015':  # DAV from 2015-11-14 ongoing
                header_csv += ('data_size_qcl', 'status_code_qcl', 'ch4_ppb_qcl', 'n2o_ppb_qcl', 'co2_ppb_qcl',
                               'h2o_ppb_qcl', 'cell_temp_K_qcl', 'cell_press_torr_qcl', 'status_word', 'vici')

        if 'lgr_fma' in data_block_order[x]:  # ToolikWetland, see Table 9 in sonicread.pdf
            header_csv += ('data_size_fma', 'status_code_fma', 'ch4_ppm_fma', 'cell_press_torr_fma', 'cell_temp_C_fma',
                           'mirror_ringdown_time_us_fma', 'calibration_flag_fma')

        if 'lgr_ino2018' in data_block_order[x]:  # CH-INO 2018 project, see Table 14 in sonicread.pdf
            header_csv += ('data_size_lgr', 'status_code_lgr',
                           'ch4_ppm_dmf_lgr', 'n2o_ppm_dmf_lgr', 'h2o_ppm_conc_lgr',
                           'ch4_ppm_conc_lgr', 'n2o_ppm_conc_lgr',
                           'cell_press_torr_lgr', 'cell_temp_C_lgr', 'ambient_temp_C_lgr',
                           'mirror_ringdown_time_us_lgr', 'fit_flag_int_lgr')

    return header_csv
