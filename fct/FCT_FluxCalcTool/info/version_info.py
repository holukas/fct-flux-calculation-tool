def version_info():
    version_info_dict = {
        'FluxCalcTool': 'v0.9.6',
        'EddyPro': 'v7.0.6 (18 Dec 2019)',
        'last edit': '02 Jun 2020',
        'source code': 'https://gitlab.ethz.ch/holukas/fct-flux-calculation-tool'
    }

    return version_info_dict
